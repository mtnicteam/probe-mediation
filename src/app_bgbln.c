/***********************************************************************
*
* Included libraries
*
***********************************************************************/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>
#define __USE_XOPEN
#include <time.h>
#include <stdbool.h>
#include "nodebase.h"

#include "lookup_library.h" 

#include "app_bgbln.h"

/* Include if needed
  #include <math.h>
  #include <stdbool.h>
  #include <regex.h>
*/

/***********************************************************************
*
* Reserved functions
*
***********************************************************************/

/* This function is called in the beginning when the node is starting up.
*
* Arguments:
*   None.
* Return values:
*   None.
*/
void node_init(void)
{
  DIAG(DIAG_LOW, "node_init(): entered the function");

  DIAG(DIAG_LOW, "node_init(): returning...");
}

/* This function is called for every input record or for every input
* file, if input is binary.
*
* Arguments:
*   None.
* Return values:
*   None.
*/
void node_process(void)
{
  DIAG(DIAG_HIGH, "node_process(): entered the function");  
  char inDate[20] = {'\0'};
  char outDate[20] = {'\0'};
  char* num_check_ptr;
  
  nb_new_record();
  
  if(strcmp(i_get_input_type(), "eoIUPS") == 0)
  {
    audit_out_type("eoIUPS-TDR-9.0");
  }
  else if(strcmp(i_get_input_type(), "eoIUCS") == 0)
  {
    audit_out_type("eoIUCS-TDR-9.0");
  }
  else if(strcmp(i_get_input_type(), "eoAINT") == 0)
  {
    audit_out_type("eoAINT-TDR-7.0");
  }
  else if(strcmp(i_get_input_type(), "eoGB") == 0)
  {
    audit_out_type("eoGB-TDR-6.0");
  }

  o_copy_input();

  strncpy(inDate,iGet("START_DATE_AND_TIME"), 10);
  DIAG(DIAG_HIGH, "node_process(): START DATE: %s", inDate);
  
  /* Check if format is correct*/
  strtol(inDate, &num_check_ptr, 10);
  if(num_check_ptr == inDate || *num_check_ptr != '\0' || 0 != format_date(inDate,"%s", outDate,"%Y%m%d%H%M",13, 0))
  {
    i_reject("INVALID", "Incorrect START_DATE_AND_TIME format");
    return;
  }
  DIAG(DIAG_HIGH, "node_process(): EL_START DATE: %s", outDate);

  o_add_field("EL_START_DATETIME", outDate);

  memset(inDate, '\0', sizeof(inDate));
  memset(outDate, '\0', sizeof(outDate)); 

  strncpy(inDate,iGet("END_DATE_AND_TIME"), 10);
  DIAG(DIAG_HIGH, "node_process(): END DATE: %s", inDate);
  
  /* Check if format is correct*/
  strtol(inDate, &num_check_ptr, 10);
  if(num_check_ptr == inDate || *num_check_ptr != '\0' || 0 != format_date(inDate,"%s", outDate,"%Y%m%d%H%M",13, 0))
  {
    i_reject("INVALID", "Incorrect END_DATE_AND_TIME format");
    return;
  }
  DIAG(DIAG_HIGH, "node_process(): EL_END DATE: %s", outDate);
   
  o_add_field("EL_END_DATETIME", outDate);
  
  nb_write_record("BLN_OUT");

  DIAG(DIAG_MEDIUM, "node_process(): Sending output");  
}

/* This function is called whenever a control record is received.
*
* Arguments:
*   None.
* Return values:
*   None.
*/
void node_control(void)
{
  DIAG(DIAG_LOW, "node_control()");
}

/* This function is called when the node commits after processing an
* input file successfully.
*
* Arguments:
*   None.
* Return values:
*   None.
*/
void node_commit(void)
{
  DIAG(DIAG_LOW, "node_commit()");


  DIAG(DIAG_HIGH, "node_commit(): Leaving the function");
}

/* This function is called in the end when the node is shutting down.
*
* Arguments:
*   None.
* Return values:
*   None.
*/
void node_end(void)
{
  DIAG(DIAG_LOW, "node_end()");
}

/* This function is called when the operator requested the flushing
* of the steram. If the node stores any records in an internal
* storage, all records should be retrieved from the storage and
* written to the output in this function.
*
* Arguments:
*   None.
* Return values:
*   None.
*/
void node_flush(void)
{
  DIAG(DIAG_LOW, "node_flush()");
}


/* This function is called if an error occurs during the processing of a
* file/record, and should be used to reset the system to the point of 
* the last commit.
*
* Arguments:
*   None.
* Return values:
*   None.
*/
void node_rollback(void)
{
  DIAG(DIAG_LOW, "node_rollback()");
}

/* This function is called whenever the node is scheduled to be executed.
*
* Arguments:
*   None.
* Return values:
*   NB_OK if the scheduled functionality is executed successfully, NB_ERROR otherwise
*/
int node_schedule(void)
{
  DIAG(DIAG_LOW, "node_schedule(): entered the function");
  DIAG(DIAG_LOW, "node_schedule(): returning NB_OK...");
  return NB_OK;
}

/* This function is called in regular intervals, about every second.
*
* Arguments:
*   None.
* Return values:
*   None.
*/
void node_timer(void)
{
  DIAG(DIAG_HIGH, "node_timer()"); 
}

/* This function is called for real-time nodes if they have to be stopped.
*
* Arguments:
*   None.
* Return values:
*   None.
*/
void node_pause(void)
{
  DIAG(DIAG_LOW, "node_pause()");
}

/* This function is called for real-time nodes when they are resumed
* after having paused.
*
* Arguments:
*   None.
* Return values:
*   None.
*/
void node_resume(void)
{
  DIAG(DIAG_LOW, "node_resume()");
}

/* This function is called upon receival of external requests.
 *
 * Arguments:
 *   None.
 * Return values:
 *   None.
 */
void node_request(void)
{
  DIAG(DIAG_LOW, "node_request()");
}

/***********************************************************************
*
* Application-specific functions
*
***********************************************************************/

/* -----------------------------------------------------------------------------
* Function  : iGet()
* Purpose   : Check Existence and Read Input Field
* Arguments : NameOfField - Name of the Input Field to be read
* Return    : Value of Field
* --------------------------------------------------------------------------- */
const char * iGet(const char *NameOfField)
{
  if(i_field_exists(NameOfField))
  {
    DIAG(DIAG_HIGH, "iGet(): %-30s = [%s]", NameOfField, i_get(NameOfField));
  }
  else
  {
    DIAG(DIAG_HIGH, "iGet(): %-30s = <Does not exist>", NameOfField);
  }
  return i_get(NameOfField);
}

/* -----------------------------------------------------------------------------
* Function  : format_date()
* Purpose   : Change date format from an input to an output
* Arguments : src - input date, pattern - input format, target - char pointer to store output,  targetPattern - target format
* Return    : 0 if successful
* --------------------------------------------------------------------------- */
int format_date(char* src, char* pattern, char* target, char* targetPattern, int targetLenth, int secondsToAdd)
{   
    if(strlen(src)==0)
    {
      DIAG(DIAG_HIGH, "format_date Failed. Empty source string.");
 
      return -2;
    }
    struct tm result;
	
    memset(&result, 0, sizeof(struct tm));
    if (strptime(src, pattern,&result) == NULL)
    {
          DIAG(DIAG_HIGH, "\nstrptime failed\n");
          DIAG(DIAG_HIGH, "'%s'\t'%s' '%s'\t'%s' '%s'\t'%s'\t'%s'\n",  "Failed. format_date returning", "source",src,
                                                                        "srcPattern",pattern,"target",target);

          target = NULL;
          return 1;
    }
    else
    {
          DIAG(DIAG_HIGH,"tm_hour:  %d\n",result.tm_hour);
          DIAG(DIAG_HIGH,"tm_min:  %d\n",result.tm_min);
          DIAG(DIAG_HIGH,"tm_sec:  %d\n",result.tm_sec);
          DIAG(DIAG_HIGH,"tm_mon:  %d\n",result.tm_mon);
          DIAG(DIAG_HIGH,"tm_mday:  %d\n",result.tm_mday);
          DIAG(DIAG_HIGH,"tm_year:  %d\n",result.tm_year);
          DIAG(DIAG_HIGH,"tm_yday:  %d\n",result.tm_yday);
          DIAG(DIAG_HIGH,"tm_wday:  %d\n",result.tm_wday);
		  if(secondsToAdd > 0)
		  {
			result.tm_sec += secondsToAdd;
            timegm(&result);
		  }
          strftime(target,targetLenth,targetPattern , &result);
          
          DIAG(DIAG_HIGH, "'%s'\t'%s' '%s'\t'%s' '%s'\t'%s'\t'%s'\n",  "Success. format_date returning", "source",
                                                                  src, "targetPattern",targetPattern,"target",target);
          return 0;
    }
}