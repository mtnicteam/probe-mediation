
/*******************************************************************************
*
* MACRO Constants
*
*******************************************************************************/
/* Diagnostic level settings */
#define DIAG_LOW    10
#define DIAG_MEDIUM 50
#define DIAG_HIGH   90

#define PASS 0
#define FAIL -1
#define LENGTH 50

/*******************************************************************************
*
* Function ProtoType declarations
*
*******************************************************************************/
const char * iGet(const char *NameOfField);
int format_date(char* src, char* pattern, char* target, char* targetPattern, int targetLenth, int secondsToAdd);
