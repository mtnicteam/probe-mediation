/***********************************************************************
*
* Included libraries
*
***********************************************************************/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>
#define __USE_XOPEN
#include <time.h>
#include <stdbool.h>
#include "nodebase.h"
#include <lookup_library.h> 
#include "app_authbln.h"

/* Include if needed
  #include <math.h>
  #include <stdbool.h>
  #include <regex.h>
*/

/***********************************************************************
*
* Reserved functions
*
***********************************************************************/

/* This function is called in the beginning when the node is starting up.
*
* Arguments:
*   None.
* Return values:
*   None.
*/
void node_init(void)
{
  DIAG(DIAG_LOW, "node_init(): entered the function");
  load_lookup_tables();
  lkup_reload_time = time(NULL);
  DIAG(DIAG_LOW, "node_init(): returning...");
}

/* This function is called for every input record or for every input
* file, if input is binary.
*
* Arguments:
*   None.
* Return values:
*   None.
*/
void node_process(void)
{
  DIAG(DIAG_HIGH, "node_process(): entered the function");
  
  nb_new_record();
  o_copy_input();

  /*process record types*/
  if(strcmp(i_get_input_type(), "eoIUPS") == 0)
  {
    if(process_eoIUPS() == FAIL)
    {
      return;
    }
  }
  else if(strcmp(i_get_input_type(), "eoNGN") == 0)
  {
    if(process_eoNGN() == FAIL)
    {
      return;
    }
  }
  else if(strcmp(i_get_input_type(), "eoIUCS") == 0)
  {
    if(process_eoIUCS() == FAIL)
    {
      return;
    }
  }
  else if(strcmp(i_get_input_type(), "eoAINT") == 0)
  {
    if(process_eoAINT() == FAIL)
    {
      return;
    }
  }
  else if(strcmp(i_get_input_type(), "eoGB") == 0)
  {
    if(process_eoGB() == FAIL)
    {
      return;
    }
  }
  else if(strcmp(i_get_input_type(), "eoMAP") == 0)
  {
    if(process_eoMAP() == FAIL)
    {
      return;
    }
  }
  else if(strcmp(i_get_input_type(), "eoLTE") == 0)
  {
    if(process_eoLTE() == FAIL)
    {
      return;
    }
  }
  else if(strcmp(i_get_input_type(), "eoIPSDR") == 0)
  {
    if(process_eoIPSDR() == FAIL)
    {
      return;
    }
  }
  else if(strcmp(i_get_input_type(), "eoTCAP") == 0)
  {
    if(process_eoTCAP() == FAIL)
    {
      return;
    }
  }
  
  if(strcmp(i_get_input_type(), "eoLTE") == 0)
  {
    nb_write_record("S1_OUT");
  }
  else
  {
    char outlink_name[100] = {0};
    sprintf(outlink_name, "%s_OUT", i_get_input_type());
    nb_write_record(outlink_name);
    if(strcmp(i_get_input_type(), "eoNGN") == 0 && 
        strcmp(o_get("EL_TYPE_CDR"), "APPEL-ABOUTI") == 0)
    {
      nb_write_record("NGN_Reporter");
    }
  }
  
  

  DIAG(DIAG_MEDIUM, "node_process(): Sending output");  
}

/* This function is called whenever a control record is received.
*
* Arguments:
*   None.
* Return values:
*   None.
*/
void node_control(void)
{
  DIAG(DIAG_LOW, "node_control()");
}

/* This function is called when the node commits after processing an
* input file successfully.
*
* Arguments:
*   None.
* Return values:
*   None.
*/
void node_commit(void)
{
  DIAG(DIAG_LOW, "node_commit()");


  DIAG(DIAG_HIGH, "node_commit(): Leaving the function");
}

/* This function is called in the end when the node is shutting down.
*
* Arguments:
*   None.
* Return values:
*   None.
*/
void node_end(void)
{
  DIAG(DIAG_LOW, "node_end()");
}

/* This function is called when the operator requested the flushing
* of the steram. If the node stores any records in an internal
* storage, all records should be retrieved from the storage and
* written to the output in this function.
*
* Arguments:
*   None.
* Return values:
*   None.
*/
void node_flush(void)
{
  DIAG(DIAG_LOW, "node_flush()");
}


/* This function is called if an error occurs during the processing of a
* file/record, and should be used to reset the system to the point of 
* the last commit.
*
* Arguments:
*   None.
* Return values:
*   None.
*/
void node_rollback(void)
{
  DIAG(DIAG_LOW, "node_rollback()");
}

/* This function is called whenever the node is scheduled to be executed.
*
* Arguments:
*   None.
* Return values:
*   NB_OK if the scheduled functionality is executed successfully, NB_ERROR otherwise
*/
int node_schedule(void)
{
  DIAG(DIAG_LOW, "node_schedule(): entered the function");
  DIAG(DIAG_LOW, "node_schedule(): returning NB_OK...");
  return NB_OK;
}

/* This function is called in regular intervals, about every second.
*
* Arguments:
*   None.
* Return values:
*   None.
*/
void node_timer(void)
{
  DIAG(DIAG_HIGH, "node_timer()"); 
  if (time(NULL) - lkup_reload_time >= 300)
  {
    DIAG(DIAG_LOW, "timer(): More than 5 minutes have passed. Reloading lookup tables");
    reload_lookup_table(LKUP_SRC_DST_IP, LKUP_SRC_DST_IP_HANDLE);
    lkup_reload_time = time(NULL);
  }
}

/* This function is called for real-time nodes if they have to be stopped.
*
* Arguments:
*   None.
* Return values:
*   None.
*/
void node_pause(void)
{
  DIAG(DIAG_LOW, "node_pause()");
}

/* This function is called for real-time nodes when they are resumed
* after having paused.
*
* Arguments:
*   None.
* Return values:
*   None.
*/
void node_resume(void)
{
  DIAG(DIAG_LOW, "node_resume()");
}

/* This function is called upon receival of external requests.
 *
 * Arguments:
 *   None.
 * Return values:
 *   None.
 */
void node_request(void)
{
  DIAG(DIAG_LOW, "node_request()");
}

/***********************************************************************
*
* Application-specific functions
*
***********************************************************************/

/* -----------------------------------------------------------------------------
* Function  : common_validation()
* Purpose   : Validate the common rules for all file types
* Arguments : value - field value
*             rej_store - rejection store
* Return    : PASS or FAIL
* --------------------------------------------------------------------------- */
int missing_validation(const char* value, const char* rej_store)
{
  if(strlen(value) == 0)
  {
    //i_reject(rej_store, value);
    return FAIL;
  }

  return PASS;
}
/* -----------------------------------------------------------------------------
* Function  : process_date()
* Purpose   : Transform input date field of epoch format into output of LMS
* Arguments : in_field_name - Name of the Input Field to be read
*             out_field_name - Name of the output field to be written
* Return    : PASS or FAIL
* --------------------------------------------------------------------------- */
int process_date(const char* in_field_name, const char* out_field_name, const char* out_format)
{
  DIAG(DIAG_HIGH, "process_date(): entered the function");  
  char inDate[20] = {'\0'};
  char outDate[25] = {'\0'};
  char rej_msg[100] = {'\0'};
  char* num_check_ptr;

  strncpy(inDate, iGet(in_field_name), 10);
  DIAG(DIAG_HIGH, "process_date(): %s: %s", in_field_name, inDate);
  
  /* Check if format is correct*/
  strtol(inDate, &num_check_ptr, 10);
  if(num_check_ptr == inDate || *num_check_ptr != '\0' || 0 != format_date(inDate, "%s", outDate, out_format, 22, 0))
  {
    sprintf(rej_msg, "Incorrect %s format", in_field_name);
    i_reject("INVALID", rej_msg);
    return FAIL;
  }
  DIAG(DIAG_HIGH, "process_date(): %s: %s", out_field_name, outDate);

  o_add_field(out_field_name, outDate);
  return PASS;
}
/* -----------------------------------------------------------------------------
* Function  : iGet()
* Purpose   : Check Existence and Read Input Field
* Arguments : NameOfField - Name of the Input Field to be read
* Return    : Value of Field
* --------------------------------------------------------------------------- */
const char * iGet(const char *NameOfField)
{
  if(i_field_exists(NameOfField))
  {
    DIAG(DIAG_HIGH, "iGet(): %-30s = [%s]", NameOfField, i_get(NameOfField));
  }
  else
  {
    DIAG(DIAG_HIGH, "iGet(): %-30s = <Does not exist>", NameOfField);
  }
  return i_get(NameOfField);
}

/* -----------------------------------------------------------------------------
* Function  : format_date()
* Purpose   : Change date format from an input to an output
* Arguments : src - input date, pattern - input format, target - char pointer to store output,  targetPattern - target format
* Return    : 0 if successful
* --------------------------------------------------------------------------- */
int format_date(char* src, char* pattern, char* target, const char* targetPattern, int targetLenth, int secondsToAdd)
{   
    if(strlen(src)==0)
    {
      DIAG(DIAG_HIGH, "format_date Failed. Empty source string.");
 
      return -2;
    }
    struct tm result;
	
    memset(&result, 0, sizeof(struct tm));
    if (strptime(src, pattern,&result) == NULL)
    {
          DIAG(DIAG_HIGH, "\nstrptime failed\n");
          DIAG(DIAG_HIGH, "'%s'\t'%s' '%s'\t'%s' '%s'\t'%s'\t'%s'\n",  "Failed. format_date returning", "source",src,
                                                                        "srcPattern",pattern,"target",target);

          target = NULL;
          return 1;
    }
    else
    {
          DIAG(DIAG_HIGH,"tm_hour:  %d\n",result.tm_hour);
          DIAG(DIAG_HIGH,"tm_min:  %d\n",result.tm_min);
          DIAG(DIAG_HIGH,"tm_sec:  %d\n",result.tm_sec);
          DIAG(DIAG_HIGH,"tm_mon:  %d\n",result.tm_mon);
          DIAG(DIAG_HIGH,"tm_mday:  %d\n",result.tm_mday);
          DIAG(DIAG_HIGH,"tm_year:  %d\n",result.tm_year);
          DIAG(DIAG_HIGH,"tm_yday:  %d\n",result.tm_yday);
          DIAG(DIAG_HIGH,"tm_wday:  %d\n",result.tm_wday);
          DIAG(DIAG_HIGH,"tm_isdst : %d\n", result.tm_isdst);
          
		  if(secondsToAdd > 0)
		  {
			result.tm_sec += secondsToAdd;
            timegm(&result);
		  }
          strftime(target,targetLenth,targetPattern , &result);
          
          DIAG(DIAG_HIGH, "'%s'\t'%s' '%s'\t'%s' '%s'\t'%s'\t'%s'\n",  "Success. format_date returning", "source",
                                                                  src, "targetPattern",targetPattern,"target",target);
          return 0;
    }
}

/* -----------------------------------------------------------------------------
* Function  : add_date_time()
* Purpose   : add common date and time fields to all output types
* Arguments : 
* Return    : PASS - FAIL
* --------------------------------------------------------------------------- */
int add_date_time()
{
  if(process_date("START_DATE_AND_TIME","DATE", "%d/%m/%Y") == FAIL)
  {
    return FAIL;
  }
  if(process_date("START_DATE_AND_TIME","HOUR", "%H:%M:%S") == FAIL)
  {
    return FAIL;
  }
  else
  {
    if(strlen(iGet("START_DATE_AND_TIME")) == 13)
    {
      DIAG(DIAG_HIGH, "add_date_time() Processing milliseconds");
      /*sprintf(out_time,"%s:%s", o_get("HOUR"), iGet("START_DATE_AND_TIME")+10);
      o_set_field("HOUR", out_time);*/
    }
  }
  return PASS;
}

/* -----------------------------------------------------------------------------
* Function  : process_eoIUPS()
* Purpose   : process eoIUPS record
* Arguments : 
* Return    : PASS - FAIL
* --------------------------------------------------------------------------- */
int process_eoIUPS()
{
  DIAG(DIAG_HIGH, "process_eoIUPS() Started Processing");

  if(missing_validation(i_get("IMSI"), "REJ_IMSI") == FAIL)
  {
    return FAIL;
  }

  if(add_date_time() == FAIL)
  {
    DIAG(DIAG_LOW, "process_eoIUPS() Filtered record for incorrect format of START_DATE_AND_TIME");
    return FAIL;
  }

  o_add_field("IMSIA", i_get("IMSI"));
  o_add_field("IMEIA", i_get("IMEI"));
  o_add_field("TECHNOLOGY", "3G");
  o_add_field("COUNTRY", i_get("SUB_COUNTRY"));

  o_add_field("MSISDNA", i_get("MSISDN"));

  //Location
  if(strcmp(i_get("DATARECORDTYPE"), "1") == 0 || strcmp(i_get("DATARECORDTYPE"), "7") == 0)
  {
    if(strcmp(i_get("DATARECORDTYPE"),"1") == 0)
    {
      o_add_field("EVENT", "IMSI_ATTACH");
    }
    else if(strcmp(i_get("DATARECORDTYPE"), "7") == 0)
    {
      o_add_field("EVENT", "ROUTING_AREA_UPDATE");
    }
     o_add_field("CID", i_get("SAC_1"));
    audit_out_type("IUPS_LOCATION");
  }
  else if(strcmp(i_get("DATARECORDTYPE"), "2") == 0) //IMSI DETACH
  {
     o_add_field("CID", i_get("SAC_3"));
    audit_out_type("IUPS_IMSI-DETACH");
  }
  else
  {
    DIAG(DIAG_HIGH, "process_eoIUPS() No Match. Returning FAIL.");
    return FAIL;
  }
  
  DIAG(DIAG_HIGH, "process_eoIUPS() Finished Processing. Returning PASS.");

  return PASS;
}

/* -----------------------------------------------------------------------------
* Function  : process_eoIUCS()
* Purpose   : process eoIUCS record
* Arguments : 
* Return    : PASS - FAIL
* --------------------------------------------------------------------------- */
int process_eoIUCS()
{
  DIAG(DIAG_HIGH, "process_eoIUCS() Started Processing");
  char call_record[3][3] = {"1", "2", "12"};
  char sms_record[2][2] = {"3", "4"};


  if(missing_validation(i_get("IMSI"), "REJ_IMSI") == FAIL)
  {
    return FAIL;
  }

  

  if(val_in_array(i_get("DATARECORDTYPE"), sizeof(call_record)/sizeof(call_record[0]), call_record) == PASS)
  {
    if(strcmp(i_get("SUCCESS"), "1") == 0)
    {
      o_add_field("EL_TYPE_CDR", "APPEL-ABOUTI");
      audit_out_type("IUCS_APPEL-ABOUTI");
    }
    else if(strcmp(i_get("SUCCESS"), "0") == 0)
    {
      o_add_field("EL_TYPE_CDR", "APPEL-NON-ABOUTI");
      audit_out_type("IUCS_APPEL-NO-ABOUTI");
      o_add_field("CAUSE", i_get("CAUSE"));
    }
    return process_call(i_get("DATARECORDTYPE"), "MSISDN", "3G", "SAC_1", "SAC_3");
  }
  else if(val_in_array(i_get("DATARECORDTYPE"), sizeof(sms_record)/sizeof(sms_record[0]), sms_record) == PASS)
  {
    o_add_field("EL_TYPE_CDR", "SMS");
    audit_out_type("IUCS_SMS");
    return process_sms(i_get("DATARECORDTYPE"), "MSISDN", "3G", "SAC_1", "SAC_3");
  }
  else if(strcmp(i_get("DATARECORDTYPE"), "9") == 0 || strcmp(i_get("LOC_UPD_TYPE"), "2") == 0)
  {
    o_add_field("EL_TYPE_CDR", "LOCATION");
    audit_out_type("IUCS_LOCATION");
    return process_loc("3G", "SAC_1", "MSISDN");
  }
  else if(strcmp(i_get("DATARECORDTYPE"), "17") == 0)
  {
    o_add_field("EL_TYPE_CDR", "IMSI_Detach");
    audit_out_type("IUCS_IMSI-DETACH");
    return process_imsi_det("MSISDN", "3G", "SAC_3");
  }
  DIAG(DIAG_HIGH, "process_eoIUCS() Could not find matching DATARECORDTYPE");

  return FAIL;
}

int val_in_array(const char* val, int size, char arr[][size])
{
  DIAG(DIAG_HIGH, "val_in_array() Started Processing %s %d", val, size);
  
  for(int i = 0; i < size; i++)
  {
    if(strcmp(arr[i], val) == 0)
    {
      DIAG(DIAG_HIGH, "val_in_array() found %s", arr[i]);
      return PASS;
    }
  }
  DIAG(DIAG_HIGH, "val_in_array() not found %s", val);
  return FAIL;
}
void add_common_fields(const char* tech, const char* cidbegin, const char* cidend)
{
  o_add_field("DURATION", i_get("CONV_TIME"));
  o_add_field("IMSIA", i_get("IMSI"));
  o_add_field("IMEIA", i_get("IMEI"));
  o_add_field("CIDBEGIN", cidbegin);
  o_add_field("CIDEND", cidend);
  o_add_field("TECHNOLOGY", tech);//"2G"
}

int process_call(const char* data_rec_val, const char* msisdn_field, const char* tech, const char* cidbegin, const char* cidend)
{
  if(add_date_time() == FAIL)
  {
    DIAG(DIAG_LOW, "process_call() Filtered record for incorrect format of START_DATE_AND_TIME");
    return FAIL;
  }

  add_common_fields(tech, i_get(cidbegin), i_get(cidend));

  switch(atoi(data_rec_val))
  {
    case 1:
      o_add_field("COUNTRY", i_get("CALLED_COUNTRY"));
    case 12:
      o_add_field("DIRECTION", "Emis");
      o_add_field("MSISDNA", i_get(msisdn_field));
      o_add_field("MSISDNB", i_get("CALLED_NO"));
      break;
    case 2:
      o_add_field("DIRECTION", "Recu");
      o_add_field("MSISDNA", i_get("CALLING_NO"));
      o_add_field("MSISDNB", i_get(msisdn_field));
      o_add_field("COUNTRY", i_get("CALLING_COUNTRY"));
      break;
    default:
      break;
  }

  return PASS;
}

int process_sms(const char* data_rec_val, const char* msisdn_field, const char* tech, const char* cidbegin, const char* cidend)
{
  if(add_date_time() == FAIL)
  {
    DIAG(DIAG_LOW, "process_sms() Filtered record for incorrect format of START_DATE_AND_TIME");
    return FAIL;
  }

  add_common_fields(tech, i_get(cidbegin), i_get(cidend));

  switch(atoi(data_rec_val))
  {
    case 3:
      o_add_field("DIRECTION", "Emis");
      o_add_field("MSISDNA", i_get(msisdn_field));

      if(strlen(i_get("CALLED_NO")) > 0)
        o_add_field("MSISDNB", i_get("CALLED_NO"));
      else if(strlen(i_get("CALLED_NO_STRING")) > 0)
        o_add_field("MSISDNB", i_get("CALLED_NO_STRING"));
      else if(strlen(i_get("GENERAL_CALLED_NUMBER")) > 0)
        o_add_field("MSISDNB", i_get("GENERAL_CALLED_NUMBER"));
      else if(strlen(i_get("SMS_TP_DA_ALPHANUMERIC")) > 0)
        o_add_field("MSISDNB", i_get("SMS_TP_DA_ALPHANUMERIC"));

      o_add_field("COUNTRY", i_get("CALLED_COUNTRY"));
      break;
    case 4:
      o_add_field("DIRECTION", "Recu");
      
      if(strlen(i_get("CALLING_NO")) > 0)
        o_add_field("MSISDNA", i_get("CALLING_NO"));
      else if(strlen(i_get("CLG_PARTY_NUM_STR")) > 0)
        o_add_field("MSISDNA", i_get("CLG_PARTY_NUM_STR"));
      else if(strlen(i_get("CALLING_NO_STRING")) > 0)
        o_add_field("MSISDNA", i_get("CALLING_NO_STRING"));
      else if(strlen(i_get("SMS_TP_OA_ALPHANUMERIC")) > 0)
        o_add_field("MSISDNA", i_get("SMS_TP_OA_ALPHANUMERIC"));
      
      o_add_field("MSISDNB", i_get(msisdn_field));
      o_add_field("COUNTRY", i_get("CALLING_COUNTRY"));
      break;
    default:
      break;
  }

  o_add_field("EL_CAUSE", i_get("CAUSE"));

  return PASS;
}

int process_loc(const char* tech, const char* cid, const char* msisdn_field)
{
  if(add_date_time() == FAIL)
  {
    DIAG(DIAG_LOW, "process_loc() Filtered record for incorrect format of START_DATE_AND_TIME");
    return FAIL;
  }

  o_add_field("IMSIA", i_get("IMSI"));
  o_add_field("IMEIA", i_get("IMEI"));
  o_add_field("MSISDNA", i_get(msisdn_field));
  o_add_field("CID", i_get(cid));
  o_add_field("TECHNOLOGY", tech);
  o_add_field("COUNTRY", i_get("SUB_COUNTRY"));
  if(strcmp(i_get("LOC_UPD_TYPE"), "2") == 0)
    o_add_field("EVENT", "IMSI_ATTACH");
  else
    o_add_field("EVENT", "LOCATION_UPDATE");
  return PASS;
}
int process_imsi_det(const char* msisdn_field, const char* tech, const char* cid)
{
  char out_msisdna[20] = {0};
  if(add_date_time() == FAIL)
  {
    DIAG(DIAG_LOW, "process_imsi_det() Filtered record for incorrect format of START_DATE_AND_TIME");
    return FAIL;
  }
  o_add_field("MSISDNA", i_get(msisdn_field));
  o_add_field("IMSIA", i_get("IMSI"));
  o_add_field("IMEIA", i_get("IMEI"));
  o_add_field("CID", i_get(cid));
  o_add_field("TECHNOLOGY", tech);
  o_add_field("COUNTRY", i_get("SUB_COUNTRY"));

  if(strlen(o_get("MSISDNA")) != 0)
  {
    sprintf(out_msisdna, "00%s", o_get("MSISDNA"));
    o_set("MSISDNA", out_msisdna);
  }

  return PASS;
}
/* -----------------------------------------------------------------------------
* Function  : process_eoAINT()
* Purpose   : process eoAINT record
* Arguments : 
* Return    : PASS - FAIL
* --------------------------------------------------------------------------- */
int process_eoAINT()
{
  DIAG(DIAG_HIGH, "process_eoAINT() Started Processing");
  char call_record[][3] = {"1", "2", "12"};
  char sms_record[][2] = {"3", "4"};


  if(missing_validation(i_get("IMSI"), "REJ_IMSI") == FAIL)
  {
    return FAIL;
  }

  if(val_in_array(i_get("DATARECORDTYPE"), sizeof(call_record)/sizeof(call_record[0]), call_record) == PASS)
  {
    if(strcmp(i_get("SUCCESS"), "1") == 0)
    {
      o_add_field("EL_TYPE_CDR", "APPEL-ABOUTI");
      audit_out_type("A_APPEL-ABOUTI");
    }
    else if(strcmp(i_get("SUCCESS"), "0") == 0)
    {
      o_add_field("EL_TYPE_CDR", "APPEL-NON-ABOUTI");
      audit_out_type("A_APPEL-NO-ABOUTI");
      o_add_field("CAUSE", i_get("CAUSE"));
    }
    return process_call(i_get("DATARECORDTYPE"), "MS_ISDN", "2G", "FIRST_CELL", "LAST_CELL");
  }
  else if(val_in_array(i_get("DATARECORDTYPE"), sizeof(sms_record)/sizeof(sms_record[0]), sms_record) == PASS)
  {
    o_add_field("EL_TYPE_CDR", "SMS");
    audit_out_type("A_SMS");
    return process_sms(i_get("DATARECORDTYPE"), "MS_ISDN", "2G", "FIRST_CELL", "LAST_CELL");
  }
  else if(strcmp(i_get("DATARECORDTYPE"), "5") == 0 || strcmp(i_get("LOC_UPD_TYPE"), "2") == 0)
  {
    o_add_field("EL_TYPE_CDR", "LOCATION");
    audit_out_type("A_LOCATION");
    return process_loc("2G", "FIRST_CELL", "MS_ISDN");
  }
  else if(strcmp(i_get("DATARECORDTYPE"), "16") == 0)
  {
    o_add_field("EL_TYPE_CDR", "IMSI_Detach");
    audit_out_type("A_IMSI-DETACH");
    return process_imsi_det("MS_ISDN", "2G", "LAST_CELL");
  }

  DIAG(DIAG_HIGH, "process_eoAINT() Could not find matching DATARECORDTYPE");
  
  return FAIL;
}

/* -----------------------------------------------------------------------------
* Function  : process_eoNGN()
* Purpose   : process eoNGN record
* Arguments : 
* Return    : PASS - FAIL
* --------------------------------------------------------------------------- */
int process_eoNGN()
{
  DIAG(DIAG_HIGH, "process_eoNGN() Started Processing");

  if(missing_validation(i_get("IMSI"), "REJ_IMSI") == FAIL)
  {
    DIAG(DIAG_LOW, "process_eoNGN() Reject for missing IMSI");
    return FAIL;
  }

  if(strcmp(i_get("DATARECORDTYPE"), "0") == 0 && strcmp(i_get("SIP_VOLTE_FLAG"), "1") == 0)
  {
    if(strcmp(i_get("SUCCESS"), "1") == 0)
    {
      o_add_field("EL_TYPE_CDR", "APPEL-ABOUTI");
      audit_out_type("eoNGN_APPEL-ABOUTI");
    }
    else if(strcmp(i_get("SUCCESS"), "0") == 0)
    {
      o_add_field("EL_TYPE_CDR", "APPEL-NON-ABOUTI");
      audit_out_type("eoNGN_APPEL-NO-ABOUTI");
      o_add_field("CAUSE", i_get("CAUSE"));
    }
  }
  else if(strcmp(i_get("DATARECORDTYPE"), "1") == 0 && strcmp(i_get("ACCESS_TECHNOLOGY"), "E-UTRAN") == 0 && 
            strcmp(i_get("REQUEST_TYPE"), "1") == 0)
  {
    o_add_field("EL_TYPE_CDR", "IMSI_DETACH");
    audit_out_type("eoNGN_IMSI_DETACH");
  }
  else {
    DIAG(DIAG_LOW, "process_eoNGN() Filtered record for DATARECORDTYPE not matched");
    return FAIL;
  }

  if( add_ip_from_field("FL_SOURCE_IP", "EL_FL_SOURCE_IP") == FAIL ||
      add_ip_from_field("FL_DESTINATION_IP", "EL_FL_DESTINATION_IP") == FAIL)
  {
    DIAG(DIAG_LOW, "process_eoNGN() Filtered record for FL_SOURCE_IP,FL_DESTINATION_IP conversion error!");
    return FAIL;
  }

  if(is_src_dst_ip_allowed(o_get("EL_FL_SOURCE_IP"), o_get("EL_FL_DESTINATION_IP")) == FAIL)
  {
    DIAG(DIAG_LOW, "process_eoNGN() Filtered record for SRC_IP/DST_IP not matched [%s] [%s]", o_get("EL_FL_SOURCE_IP"), o_get("EL_FL_DESTINATION_IP"));
    return FAIL;
  }

  o_add_field("MSISDNA", i_get("CALLING_PARTY"));
  o_add_field("IMSIA", i_get("IMSI"));
  o_add_field("IMEIA", i_get("IMEI"));
  o_add_field("MSISDNB", i_get("CALLED_PARTY"));
  o_add_field("IMSIB", i_get("CALLED_IMSI"));
  o_add_field("IMEIB", i_get("TERM_IMEI"));
  o_add_field("CIDBEGIN", i_get("OriginatingUserLocation"));
  o_add_field("CIDEND", i_get("TerminatingUserlocation"));
  o_add_field("TECHNOLOGYA", i_get("OriginatingPartyTechnology"));
  o_add_field("TECHNOLOGYB", i_get("TERM_ACCESS_TECHNOLOGY"));
  o_add_field("COUNTRYA ", i_get("CALLING_COUNTRY_NAME"));
  o_add_field("COUNTRYB", i_get("CALLED_COUNTRY_NAME"));

  if(add_date_time() == FAIL)
  {
    DIAG(DIAG_LOW, "process_eoNGN() Filtered record for incorrect format of START_DATE_AND_TIME");
    return FAIL;
  }

  char el_date[50] = {0};
  sprintf(el_date, "%s %s", o_get("DATE"), o_get("HOUR"));
  o_add_field("EL_DATE", el_date);

  if(strlen(i_get("SIP_Q850_REASON_CAUSE")) != 0) {
    o_add_field("EL_CAUSE", i_get("SIP_Q850_REASON_CAUSE"));
    o_add_field("CAUSE_TYPE", "Q.931");
  } else if(strlen(i_get("SIP_REASON_CAUSE")) != 0) {
    o_add_field("EL_CAUSE", i_get("SIP_REASON_CAUSE"));
    o_add_field("CAUSE_TYPE", "SIP");
  } else if(strlen(i_get("SIP_SMS_TP_FAILURE_CAUSE")) != 0) {
    o_add_field("EL_CAUSE", i_get("SIP_SMS_TP_FAILURE_CAUSE"));
    o_add_field("CAUSE_TYPE", "SMS_TP");
  } else if(strlen(i_get("SIP_SMS_RP_ERR_CAUSE")) != 0) {
    o_add_field("EL_CAUSE", i_get("SIP_SMS_RP_ERR_CAUSE"));
    o_add_field("CAUSE_TYPE", "SMS_RP");
  } else if(strlen(i_get("FL_SIP_RESPONSE")) != 0) {
    o_add_field("EL_CAUSE", i_get("FL_SIP_RESPONSE"));
    o_add_field("CAUSE_TYPE", "SIP");
  } else if(strlen(i_get("LL_SIP_RESPONSE")) != 0) {
    o_add_field("EL_CAUSE", i_get("LL_SIP_RESPONSE"));
    o_add_field("CAUSE_TYPE", "SIP");
  }

  DIAG(DIAG_HIGH, "process_eoNGN() End Processing output");
  
  return PASS;
}

int process_eoTCAP()
{
  DIAG(DIAG_HIGH, "process_eoTCAP() Started Processing");
  if(missing_validation(i_get("IMSI"), "REJ_IMSI") == FAIL)
  {
    return FAIL;
  }
  DIAG(DIAG_HIGH, "process_eoTCAP() CALLING_NO comparison %d", strncmp(i_get("CALLING_NO"),"212", 3));
  DIAG(DIAG_HIGH, "process_eoTCAP() VLR_NUMBER comparison %d", strncmp(i_get("VLR_NUMBER"),"212", 3));
  if(eoTCAP_VLD1() || eoTCAP_VLD2("1", "212661", "21266399", "212661") ||
    eoTCAP_VLD2("2", "21266399", "21266399", "212661"))
  {
    if(strcmp(i_get("SUCCESS"), "1") == 0)
    {
      char cellid[200] = {0};
      char capcell[100] = {0};
      
      sprintf(cellid, "%s-", i_get("PLMN_ID"));
      
      if(strlen(i_get("CAP_CELL_IDENTITY")) > 0)
      {
        sprintf(capcell, "%d", (int)strtol(i_get("CAP_CELL_IDENTITY"), NULL, 16));
        strcat(cellid, capcell);
      }
      add_common_fields("ALL Core", cellid, cellid);
      audit_out_type("TCAP-APPEL-ABOUTI");
    }
    else if(strcmp(i_get("SUCCESS"), "0") == 0)
    {
      audit_out_type("TCAP_APPEL-NO-ABOUTI");
      o_add_field("CAUSE", i_get("CAUSE"));
      o_add_field("CIDBEGIN", i_get("FIRST_CELL"));
      o_add_field("CIDEND", i_get("LAST_CELL"));
      add_common_fields("ALL Core", i_get("FIRST_CELL"), i_get("LAST_CELL"));
    }

    if(add_date_time() == FAIL)
    {
      DIAG(DIAG_LOW, "process_eoTCAP() Filtered record for incorrect format of START_DATE_AND_TIME");
      return FAIL;
    }

    

    if(strcmp(i_get("DATARECORDTYPE"), "1") == 0)
    {
      o_add_field("DIRECTION", "Emis");
      o_add_field("MSISDNA", i_get("MSISDN"));
      o_add_field("MSISDNB", i_get("CALLED_NO"));
      o_add_field("COUNTRY", i_get("CALLED_COUNTRY"));
    }
    else if(strcmp(i_get("DATARECORDTYPE"), "2") == 0)
    {
      o_add_field("DIRECTION", "Recu");
      o_add_field("MSISDNA", i_get("CALLING_NO"));
      o_add_field("MSISDNB", i_get("MSISDN"));
      o_add_field("COUNTRY", i_get("CALLING_COUNTRY"));
      o_add_field("CAUSE", i_get("CAUSE"));
    }
    
    DIAG(DIAG_HIGH, "process_eoTCAP() End Processing output");
    return PASS;  
  }  

  DIAG(DIAG_HIGH, "process_eoTCAP() End Processing No Match");
  return FAIL;
}

int eoTCAP_VLD1()
{
  return (strcmp(i_get("DATARECORDTYPE"), "1") == 0 || strcmp(i_get("DATARECORDTYPE"), "2") == 0) && 
        strncmp(i_get("CALLING_NO"), "212", 3) == 0 && strncmp(i_get("VLR_NUMBER"),"212", 3) != 0;
}

int eoTCAP_VLD2(const char* DATA_RECORD, const char* CALLING_PARTY, const char* CALLED_PARTY, const char* VLR_NUMBER)
{
  return strcmp(i_get("DATARECORDTYPE"), DATA_RECORD) == 0 &&  
    strncmp(i_get("SCCP_CALLING_PARTY"), CALLING_PARTY, strlen(CALLING_PARTY)) == 0 && 
    strncmp(i_get("SCCP_CALLED_PARTY"), CALLED_PARTY, strlen(CALLED_PARTY)) == 0 &&
    strncmp(i_get("VLR_NUMBER"), VLR_NUMBER, strlen(VLR_NUMBER)) == 0;
}

/* -----------------------------------------------------------------------------
* Function  : process_eoGB()
* Purpose   : process eoGB record
* Arguments : 
* Return    : PASS - FAIL
* --------------------------------------------------------------------------- */
int process_eoGB()
{
  DIAG(DIAG_HIGH, "process_eoGB() Started Processing.");

  

  DIAG(DIAG_HIGH, "process_eoGB() Finished Processing. Returning.");
  return FAIL;
}

int process_eoIPSDR()
{
  DIAG(DIAG_HIGH, "process_eoIPSDR() Started Processing.");
  audit_out_type("IPSDR_DATA");
  if(missing_validation(i_get("IMSI"), "REJ_IMSI") == FAIL)
  {
    return FAIL;
  }

  if(add_date_time() == FAIL)
  {
    DIAG(DIAG_LOW, "process_eoIPSDR() Filtered record for incorrect format of START_DATE_AND_TIME");
    return FAIL;
  }

  o_add_field("MSISDNA", i_get("MSISDN"));
  o_add_field("IMSIA", i_get("IMSI"));
  o_add_field("IMEIA", i_get("IMEI"));
  o_add_field("CID", i_get("LOCATION"));
  //o_add_field("TECHNOLOGY", "2G");
  o_add_field("APN", i_get("APN"));
  o_add_field("COUNTRY", i_get("SUB_COUNTRY"));

  add_ip_from_field("PDP_ADDRESS_V4", "IPCLIENT");

  if(strcmp(i_get("RADIO_ACCESS_TYPE"), "1") == 0 || strcmp(i_get("RADIO_ACCESS_TYPE"), "5") == 0)
  {
    o_add_field("TECHNOLOGY", "3G");
  }
  else if(strcmp(i_get("RADIO_ACCESS_TYPE"), "2") == 0) 
  {
    o_add_field("TECHNOLOGY", "2G");
  }
  else if(strcmp(i_get("RADIO_ACCESS_TYPE"), "6") == 0) 
  {
    o_add_field("TECHNOLOGY", "4G");
  }
  else
  {
    o_add_field("TECHNOLOGY", "ALL_CORE");
  }

  DIAG(DIAG_HIGH, "process_eoIPSDR() Finished Processing. Returning.");
  return PASS;
}

/* -----------------------------------------------------------------------------
* Function  : process_eoMAP()
* Purpose   : process eoMAP record
* Arguments : 
* Return    : PASS - FAIL
* --------------------------------------------------------------------------- */
int process_eoMAP()
{
  DIAG(DIAG_HIGH, "process_eoMAP() Started Processing.");

  if(missing_validation(i_get("IMSI"), "REJ_IMSI") == FAIL)
  {
    return FAIL;
  }

  if(add_date_time() == FAIL)
  {
    DIAG(DIAG_LOW, "process_eoMAP() Filtered record for incorrect format of START_DATE_AND_TIME");
    return FAIL;
  }

  o_add_field("IMSIA", i_get("IMSI"));
  o_add_field("IMEIA", i_get("IMEI"));
  o_add_field("TECHNOLOGY", "2G");
  o_add_field("COUNTRY", i_get("NET_OPERATOR_NAME"));
  o_add_field("CIDBEGIN", i_get("FIRST_CELL"));
  o_add_field("CIDEND", i_get("LAST_CELL"));

  //SMS
  if(strcmp(i_get("DATARECORDTYPE"), "9") == 0 || strcmp(i_get("DATARECORDTYPE"), "10") == 0)
  {
    switch(atoi(i_get("DATARECORDTYPE")))
    {
      case 9:
        o_add_field("EL_DIRECTION", "Emis");
        o_add_field("MSISDNA", i_get("MS_ISDN"));

        if(strlen(i_get("TP_DA_ALPHANUM")) > 0)
          o_add_field("MSISDNB", i_get("TP_DA_ALPHANUM"));
        else if(strlen(i_get("TP_DA")) > 0)
          o_add_field("MSISDNB", i_get("TP_DA"));
        break;
      case 10:
        o_add_field("EL_DIRECTION", "Recu");
        
        if(strlen(i_get("TP_OA_ALPHANUM")) > 0)
          o_add_field("MSISDNA", i_get("TP_OA_ALPHANUM"));
        else if(strlen(i_get("TP_OA")) > 0)
          o_add_field("MSISDNA", i_get("TP_OA"));
        
        o_add_field("MSISDNB", i_get("MS_ISDN"));
        break;
      default:
        break;
    }

    audit_out_type("MAP_SMS");
  }
  else if(strcmp(i_get("DATARECORDTYPE"), "3") == 0 || strcmp(i_get("DATARECORDTYPE"), "4") == 0) //IMSI DETACH
  {
    if(strcmp(i_get("DATARECORDTYPE"), "3") == 0)
    {
      o_add_field("MSISDNA", i_get("GTCALLING_NO"));
    }
    else
    {
      if(strlen(i_get("TP_OA_ALPHANUM")) > 0)
        o_add_field("MSISDNA", i_get("TP_OA_ALPHANUM"));
      else
        o_add_field("MSISDNA", i_get("TP_OA"));
    }
    
    audit_out_type("MAP_IMSI-DETACH");
  }
  else
  {
    DIAG(DIAG_HIGH, "process_eoMAP() No Match. Returning FAIL.");
    return FAIL;
  }

  DIAG(DIAG_HIGH, "process_eoMAP() Finished Processing. Returning PASS.");
  return PASS;
}

/* -----------------------------------------------------------------------------
* Function  : process_eoLTE()
* Purpose   : process eoLTE record
* Arguments : 
* Return    : PASS - FAIL
* --------------------------------------------------------------------------- */
int process_eoLTE()
{
  DIAG(DIAG_HIGH, "process_eoLTE() Started Processing.");

  if(missing_validation(i_get("IMSI"), "REJ_IMSI") == FAIL)
  {
    DIAG(DIAG_LOW, "process_eoLTE() Filtered record for missing IMSI");
    return FAIL;
  }

  if(add_date_time() == FAIL)
  {
    DIAG(DIAG_LOW, "process_eoLTE() Filtered record for incorrect format of START_DATE_AND_TIME");
    return FAIL;
  }

  if(strcmp(i_get("DATARECORDTYPE"), "1") == 0 || strcmp(i_get("DATARECORDTYPE"), "3") == 0)
  {
    if(strcmp(i_get("DATARECORDTYPE"), "1") == 0)
      o_add_field("EVENT", "IMSI_ATTACH");
    else if(strcmp(i_get("DATARECORDTYPE"), "3") == 0)
      o_add_field("EVENT", "TRACKING_AREA_UPDATE");

    audit_out_type("S1_LOCATION");
  }
  else if(strcmp(i_get("DATARECORDTYPE"), "2") == 0)
  {
    audit_out_type("S1_IMSI-DETACH");
  }
  else
  {
    DIAG(DIAG_HIGH, "process_eoLTE() No Match. Returning FAIL.");
    return FAIL;
  }

  o_add_field("MSISDNA", i_get("MSISDN"));
  o_add_field("IMSIA", i_get("IMSI"));
  o_add_field("IMEIA", i_get("IMEI"));
  o_add_field("CID", i_get("SOURCE_CELL"));
  o_add_field("TECHNOLOGY", "2G");
  o_add_field("COUNTRY", i_get("SUB_COUNTRY"));

  DIAG(DIAG_HIGH, "process_eoLTE() Finished Processing. Returning PASS.");
  return PASS;
}

/* -----------------------------------------------------------------------------
* Function  : get_el_cause()
* Purpose   : Get eoAINT mapped string for EL_CAUSE
* Arguments : int cause - the value of incoming cause
* Return    : Mapped string
* --------------------------------------------------------------------------- */
char* get_el_cause(int cause)
{
  DIAG(DIAG_HIGH, "get_el_cause() input [%d]", cause);
  switch(cause)
  {
    case 89: return "E///SlcFR";
    case 90: return "E///SlcHR";
    case 91: return "E///SlcOLFR";
    case 92: return "E///SlcOLHR";
    default: return "";
  }   
}

/* -----------------------------------------------------------------------------
* Function  : get_el_IUPS_cause()
* Purpose   : Get eoIUPS/eoGB mapped string for EL_CAUSE
* Arguments : int cause - the value of incoming cause
* Return    : Mapped string
* --------------------------------------------------------------------------- */
char* get_el_IUPS_cause(int cause)
{
  DIAG(DIAG_HIGH, "get_el_IUPS_cause() input [%d]", cause);
  switch(cause)
  {
    case 8: return "SM_OperDetBarr";
    case 26: return "SM_InsRes";
    case 27: return "SM_MissUnApn";
    case 28: return "SM_UnPdpAddTyp";
    case 29: return "SM_UAutFail";
    case 30: return "SM_ActRejGgsn";
    case 21: return "SM_ActRejUnsp";
    case 32: return "SM_SOptNS";
    case 33: return "SM_ReqSONS";
    case 34: return "SM_SOptTempOut";
    case 35: return "SM_NSapiUsed";
    case 111: return "SM_PErrUnsp";
    default: return "";
  }   
}

/* -----------------------------------------------------------------------------
* Function  : get_AINT_start_event()
* Purpose   : Get eoAINT mapped string for start event
* Arguments : int serv_type - the value of incoming type
* Return    : Mapped string
* --------------------------------------------------------------------------- */
char* get_AINT_start_event(int serv_type)
{
  DIAG(DIAG_HIGH, "get_AINT_start_event() input [%d]", serv_type);
  switch(serv_type)
  {
    case 1: return "CMSRQ_NC_Phx";
    case 4: return "CMSRQ_SMS_Phx";
    case 8: return "CMSRQ_SS_Phx";
    case 2: return "CMSRQ_EC_Phx";
    default: return "";
  } 
}

/* -----------------------------------------------------------------------------
* Function  : get_data_rec_type()
* Purpose   : Get eoIUCS mapped string for data rec type
* Arguments : int rec_type - the value of incoming type
* Return    : Mapped string
* --------------------------------------------------------------------------- */
char* get_data_rec_type(int rec_type)
{
  DIAG(DIAG_HIGH, "get_data_rec_type() input [%d]", rec_type);
  switch(rec_type)
  {
    case 9: return "LUREQ_IA_Phx";
    case 1: return "CMSRQ_NC_Phx";
    case 12: return "CMSRQ_EC_Phx";
    case 10: return "CMSRQ_SS_Phx";
    case 3: return "CMSRQ_SMS_Phx";
    default: return "";
  } 
}

/* -----------------------------------------------------------------------------
* Function  : get_eoGB_rec_type()
* Purpose   : Get eoGB mapped string for data rec type
* Arguments : int rec_type - the value of incoming type
* Return    : Mapped string
* --------------------------------------------------------------------------- */
char* get_eoGB_rec_type(int rec_type)
{
  DIAG(DIAG_HIGH, "get_eoGB_rec_type() input [%d]", rec_type);
  switch(rec_type)
  {
    case 1: return "1";
    case 4: return "17";
    case 2: return "24";
    case 3: return "26";
    default: return "";
  } 
}

/* -----------------------------------------------------------------------------
* Function  : extract_add_field()
* Purpose   : Extract substring after the first occurence of a separator and add field to output record
* Arguments : field_val: input field value - sep: separator - sep_sz: length of sep string - out_field: name of output field
* Return    : void
* --------------------------------------------------------------------------- */
void extract_add_field(const char* field_val, const char* sep, int sep_sz, const char* out_field)
{
  char* sep_str;
  sep_str = strstr(field_val, sep);
  DIAG(DIAG_HIGH, "extract_add_field() input [%s]", field_val);
  if(sep_str != NULL)
  {
    o_add_field(out_field, sep_str+sep_sz);
  }
  else
  {
    o_add_field(out_field, field_val);
  }
}

/* -----------------------------------------------------------------------------
* Function  : load_lookup_tables()
* Purpose   : Initializes all the lookup table handles
* Arguments : None
* Return    : None
* --------------------------------------------------------------------------- */
void load_lookup_tables(void)
{  
  DIAG(DIAG_MEDIUM, "load_lookup_tables(): entered the function");

  memset(LK_SERVER_NAME, 0, sizeof(LK_SERVER_NAME));
  strcpy(LK_SERVER_NAME,nb_get_parameter_string("LookupServer"));
  if (strlen(LK_SERVER_NAME) != 0) 
  {   
    DIAG(DIAG_HIGH, "load_lookup_tables(): LK_SERVER_NAME: %s", LK_SERVER_NAME);
  } 
  else 
  {
    DIAG(DIAG_HIGH, "Parameter LookupServer is empty");     
    nb_abort();
  }

    /* load lookup server */
  nb_load_library("server_lookup");

  /* initialise lookup server */
  if(ls_initialize()) 
  {
    DIAG(DIAG_HIGH, "load_lookup_tables(): ls_initialize failed, aborting");    
    nb_abort();     
  } 
  else 
  {
    DIAG(DIAG_MEDIUM,"load_lookup_tables(): ls_initialize OK");
  }

  LKUP_SRC_DST_IP_HANDLE = get_lookup_handle(LK_SERVER_NAME, LKUP_SRC_DST_IP);
  
  DIAG(DIAG_HIGH, "load_lookup_tables(): Exiting function...");
}


/* -----------------------------------------------------------------------------
* Function  : get_lookup_handle()
* Purpose   : This function accepts lookup server name and table name and 
*             returns lookup table handle
* Arguments : lookup server name , lookup table name
* Return    : handle for the lookup table
* --------------------------------------------------------------------------- */
int get_lookup_handle(const char *server_name,const char *table_name)
{
  int handle;
  
  DIAG(DIAG_HIGH, "get_lookup_handle(): Trying to get hanedle for '%s' table" , table_name);

  handle = ls_lookup_get_table((char *)server_name,(char *)table_name);
  if (handle < 0)
  {
    DIAG(DIAG_HIGH, "get_lookup_handle(): Failed to get table handle %s",table_name);        
    nb_abort();   
  }
  
  DIAG(DIAG_HIGH, "get_lookup_handle(): hanedle for '%s' table is found" , table_name);
  
  return handle;
}

/* -----------------------------------------------------------------------------
* Function  : reload_lookup_table()
* Purpose   : Reloads Lookup Table. Warning message sent when reload fails.
* Arguments : Table Name and Handle
* Return    : None
* --------------------------------------------------------------------------- */
void reload_lookup_table(char *tableName, int tableHandle)
{
  DIAG(DIAG_HIGH, "reloadLookupTable(): Reloading Table [%s] with Handle [%d]", tableName, tableHandle);
  if(ls_reload_table(tableHandle) == -1)
  {
    DIAG(DIAG_LOW, "reloadLookupTable(): Failed reloading Table [%s] with Handle [%d]!", tableName, tableHandle);    
  }
  else
  {
    DIAG(DIAG_HIGH, "reloadLookupTable(): Table [%s] with Handle [%d] reloaded successfully.", tableName, tableHandle);
  }
}

int is_src_dst_ip_allowed(const char* SRC_IP, const char* DST_IP)
{
  char val_dst_ip[100]={'\0'};
  DIAG(DIAG_HIGH, "is_src_dst_ip_allowed(): searching SRC_IP: [%s] and matching DST_IP: [%s]", SRC_IP, DST_IP);

  ls_lookup_first(LKUP_SRC_DST_IP_HANDLE, SRC_IP );
  if ( nb_error() )
  {
    /* Error occured during lookup */
    DIAG(DIAG_LOW, "is_src_dst_ip_allowed(): ls_lookup function call for table %d ('%s') produced an error.", LKUP_SRC_DST_IP_HANDLE, LKUP_SRC_DST_IP);
    nb_abort();
  }
  do
  {
    if ( get_ls_match_length() != 0 )
    {
      /* Match found */
      strcpy(val_dst_ip, ls_lookup_value(1));
      DIAG(DIAG_HIGH, "is_src_dst_ip_allowed(): found a match for in table %d ('%s') with value: [%s]", LKUP_SRC_DST_IP_HANDLE, LKUP_SRC_DST_IP, val_dst_ip);
      if(strcmp(val_dst_ip, DST_IP) == 0)
      {
        DIAG(DIAG_HIGH, "is_src_dst_ip_allowed(): found a match for in table %d ('%s')", LKUP_SRC_DST_IP_HANDLE, LKUP_SRC_DST_IP);
        return PASS;
      }
    }
  } while( strlen(ls_lookup_next()) > 0);
  return FAIL;
}

int add_ip_from_field(const char* INPUT_FIELD, const char* TARGET_FIELD)
{
  int ip_len = strlen(i_get(INPUT_FIELD));
  DIAG(DIAG_MEDIUM, "add_ip_from_field() INPUT IP VALUE:[%s]", i_get(INPUT_FIELD));

  if(ip_len >= 8)
  {
    char hexipaddr[10] = {0};
    unsigned int a, b, c, d;
    strncpy(hexipaddr, i_get(INPUT_FIELD)+ip_len-8, 8);
    if(sscanf(hexipaddr, "%2x%2x%2x%2x", &a, &b, &c, &d) != 4)
    {
      DIAG(DIAG_MEDIUM, "add_ip_from_field() Unable to process %s.", INPUT_FIELD);
      return FAIL;
    }
    else
    {
      char decipaddr[20] = {0};
      sprintf(decipaddr, "%u.%u.%u.%u", a, b, c, d);
      o_add_field(TARGET_FIELD, decipaddr);
      return PASS;
    }
  }
  return FAIL;
}
