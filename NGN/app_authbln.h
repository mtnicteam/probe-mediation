
/*******************************************************************************
*
* MACRO Constants
*
*******************************************************************************/
/* Diagnostic level settings */
#define DIAG_LOW    10
#define DIAG_MEDIUM 50
#define DIAG_HIGH   90

#define PASS 0
#define FAIL -1
#define LENGTH 50
/*******************************************************************************
*
* Lookup Table Handles
*
*******************************************************************************/
char LK_SERVER_NAME[LENGTH];
char* LKUP_SRC_DST_IP = "SOURCE_DESTINATION_IP";
int LKUP_SRC_DST_IP_HANDLE;
long lkup_reload_time;

/*******************************************************************************
*
* Function ProtoType declarations
*
*******************************************************************************/
const char * iGet(const char *NameOfField);
int format_date(char* src, char* pattern, char* target, const char* targetPattern, int targetLenth, int secondsToAdd);
int process_eoIUPS();
int process_eoIUCS();
int process_eoAINT();
int process_eoGB();
int process_eoMAP();
int process_eoLTE();
int process_eoIPSDR();
int process_eoTCAP();
int process_eoNGN();
int process_date(const char* in_field_name, const char* out_field_name, const char* out_format);
char* get_el_cause(int cause);
char* get_el_IUPS_cause(int cause);
char* get_AINT_start_event(int serv_type);
char* get_data_rec_type(int rec_type);
void extract_add_field(const char* field_val, const char* sep, int sep_sz, const char* out_field);
char* get_eoGB_rec_type(int rec_type);
int val_in_array(const char* val, int size, char arr[][size]);
int process_call(const char* data_rec_val, const char* msisdn_field, const char* tech, const char* cidbegin, const char* cidend);
int process_sms(const char* data_rec_val, const char* msisdn_field, const char* tech, const char* cidbegin, const char* cidend);
int process_loc(const char* tech, const char* cid, const char* msisdn_field);
void add_common_fields(const char* tech, const char* cidbegin, const char* cidend);
int process_imsi_det(const char* msisdn_field, const char* tech, const char* cid);
void load_lookup_tables(void);
int get_lookup_handle(const char *server_name,const char *table_name);
void reload_lookup_table(char *tableName, int tableHandle);
int is_src_dst_ip_allowed(const char* SRC_IP, const char* DST_IP);
int add_ip_from_field(const char* INPUT_FIELD, const char* TARGET_FIELD);
int eoTCAP_VLD1();
int eoTCAP_VLD2(const char* DATA_RECORD, const char* CALLING_PARTY, const char* CALLED_PARTY, const char* VLR_NUMBER);
