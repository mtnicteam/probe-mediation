
/*******************************************************************************
*
* MACRO Constants
*
*******************************************************************************/
/* Diagnostic level settings */
#define DIAG_LOW    10
#define DIAG_MEDIUM 50
#define DIAG_HIGH   90

#define PASS 0
#define FAIL -1
#define LENGTH 50

/*******************************************************************************
*
* Function ProtoType declarations
*
*******************************************************************************/
const char * iGet(const char *NameOfField);
int format_date(char* src, char* pattern, char* target, const char* targetPattern, int targetLenth, int secondsToAdd);
int process_eoIUPS();
int process_eoIUCS();
int process_eoAINT();
int process_eoGB();
int process_eoMAP();
int process_eoLTE();
int process_date(const char* in_field_name, const char* out_field_name, const char* out_format);
char* get_el_cause(int cause);
char* get_el_IUPS_cause(int cause);
char* get_AINT_start_event(int serv_type);
char* get_data_rec_type(int rec_type);
void extract_add_field(const char* field_val, const char* sep, int sep_sz, const char* out_field);
char* get_eoGB_rec_type(int rec_type);