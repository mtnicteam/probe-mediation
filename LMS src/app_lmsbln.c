/***********************************************************************
*
* Included libraries
*
***********************************************************************/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>
#define __USE_XOPEN
#include <time.h>
#include <stdbool.h>
#include "nodebase.h"

#include "app_lmsbln.h"

/* Include if needed
  #include <math.h>
  #include <stdbool.h>
  #include <regex.h>
*/

/***********************************************************************
*
* Reserved functions
*
***********************************************************************/

/* This function is called in the beginning when the node is starting up.
*
* Arguments:
*   None.
* Return values:
*   None.
*/
void node_init(void)
{
  DIAG(DIAG_LOW, "node_init(): entered the function");

  DIAG(DIAG_LOW, "node_init(): returning...");
}

/* This function is called for every input record or for every input
* file, if input is binary.
*
* Arguments:
*   None.
* Return values:
*   None.
*/
void node_process(void)
{
  DIAG(DIAG_HIGH, "node_process(): entered the function");
  
  nb_new_record();
  o_copy_input();

  /*process date time*/
  if(process_date("START_DATE_AND_TIME","EL_START_DATETIME", "%Y%m%d%H%M") == FAIL)
  {
    return;
  }
  if(process_date("END_DATE_AND_TIME","EL_END_DATETIME", "%Y%m%d%H%M") == FAIL)
  {
    return;
  }

  /*process record types*/
  if(strcmp(i_get_input_type(), "eoIUPS") == 0)
  {
    if(process_eoIUPS() == FAIL)
    {
      return;
    }
  }
  else if(strcmp(i_get_input_type(), "eoIUCS") == 0)
  {
    if(process_eoIUCS() == FAIL)
    {
      return;
    }
    audit_out_type("eoIUCS-TDR-9.0");
  }
  else if(strcmp(i_get_input_type(), "eoAINT") == 0)
  {
    if(process_eoAINT() == FAIL)
    {
      return;
    }
    audit_out_type("eoAINT-TDR-7.0");
  }
  else if(strcmp(i_get_input_type(), "eoGB") == 0)
  {
    if(process_eoGB() == FAIL)
    {
      return;
    }
    audit_out_type("eoGB-TDR-6.0");
  }
  else if(strcmp(i_get_input_type(), "eoMAP") == 0)
  {
    if(process_eoMAP() == FAIL)
    {
      return;
    }
  }
  else if(strcmp(i_get_input_type(), "eoLTE") == 0)
  {
    if(process_eoLTE() == FAIL)
    {
      return;
    }
  }
  
  if(strcmp(i_get_input_type(), "eoLTE") == 0)
  {
    nb_write_record("S1_OUT");
  }
  else
  {
    char outlink_name[100] = {0};
    sprintf(outlink_name, "%s_OUT", i_get_input_type());
    nb_write_record(outlink_name);
  }
  
  

  DIAG(DIAG_MEDIUM, "node_process(): Sending output");  
}

/* This function is called whenever a control record is received.
*
* Arguments:
*   None.
* Return values:
*   None.
*/
void node_control(void)
{
  DIAG(DIAG_LOW, "node_control()");
}

/* This function is called when the node commits after processing an
* input file successfully.
*
* Arguments:
*   None.
* Return values:
*   None.
*/
void node_commit(void)
{
  DIAG(DIAG_LOW, "node_commit()");


  DIAG(DIAG_HIGH, "node_commit(): Leaving the function");
}

/* This function is called in the end when the node is shutting down.
*
* Arguments:
*   None.
* Return values:
*   None.
*/
void node_end(void)
{
  DIAG(DIAG_LOW, "node_end()");
}

/* This function is called when the operator requested the flushing
* of the steram. If the node stores any records in an internal
* storage, all records should be retrieved from the storage and
* written to the output in this function.
*
* Arguments:
*   None.
* Return values:
*   None.
*/
void node_flush(void)
{
  DIAG(DIAG_LOW, "node_flush()");
}


/* This function is called if an error occurs during the processing of a
* file/record, and should be used to reset the system to the point of 
* the last commit.
*
* Arguments:
*   None.
* Return values:
*   None.
*/
void node_rollback(void)
{
  DIAG(DIAG_LOW, "node_rollback()");
}

/* This function is called whenever the node is scheduled to be executed.
*
* Arguments:
*   None.
* Return values:
*   NB_OK if the scheduled functionality is executed successfully, NB_ERROR otherwise
*/
int node_schedule(void)
{
  DIAG(DIAG_LOW, "node_schedule(): entered the function");
  DIAG(DIAG_LOW, "node_schedule(): returning NB_OK...");
  return NB_OK;
}

/* This function is called in regular intervals, about every second.
*
* Arguments:
*   None.
* Return values:
*   None.
*/
void node_timer(void)
{
  DIAG(DIAG_HIGH, "node_timer()"); 
}

/* This function is called for real-time nodes if they have to be stopped.
*
* Arguments:
*   None.
* Return values:
*   None.
*/
void node_pause(void)
{
  DIAG(DIAG_LOW, "node_pause()");
}

/* This function is called for real-time nodes when they are resumed
* after having paused.
*
* Arguments:
*   None.
* Return values:
*   None.
*/
void node_resume(void)
{
  DIAG(DIAG_LOW, "node_resume()");
}

/* This function is called upon receival of external requests.
 *
 * Arguments:
 *   None.
 * Return values:
 *   None.
 */
void node_request(void)
{
  DIAG(DIAG_LOW, "node_request()");
}

/***********************************************************************
*
* Application-specific functions
*
***********************************************************************/

/* -----------------------------------------------------------------------------
* Function  : process_date()
* Purpose   : Transform input date field of epoch format into output of LMS
* Arguments : in_field_name - Name of the Input Field to be read
*             out_field_name - Name of the output field to be written
* Return    : PASS or FAIL
* --------------------------------------------------------------------------- */
int process_date(const char* in_field_name, const char* out_field_name, const char* out_format)
{
  DIAG(DIAG_HIGH, "process_date(): entered the function");  
  char inDate[20] = {'\0'};
  char outDate[25] = {'\0'};
  char rej_msg[100] = {'\0'};
  char* num_check_ptr;

  strncpy(inDate, iGet(in_field_name), 10);
  DIAG(DIAG_HIGH, "process_date(): %s: %s", in_field_name, inDate);
  
  /* Check if format is correct*/
  strtol(inDate, &num_check_ptr, 10);
  if(num_check_ptr == inDate || *num_check_ptr != '\0' || 0 != format_date(inDate, "%s", outDate, out_format, 22, 0))
  {
    sprintf(rej_msg, "Incorrect %s format", in_field_name);
    i_reject("INVALID", rej_msg);
    return FAIL;
  }
  DIAG(DIAG_HIGH, "process_date(): %s: %s", out_field_name, outDate);

  o_add_field(out_field_name, outDate);
  return PASS;
}
/* -----------------------------------------------------------------------------
* Function  : iGet()
* Purpose   : Check Existence and Read Input Field
* Arguments : NameOfField - Name of the Input Field to be read
* Return    : Value of Field
* --------------------------------------------------------------------------- */
const char * iGet(const char *NameOfField)
{
  if(i_field_exists(NameOfField))
  {
    DIAG(DIAG_HIGH, "iGet(): %-30s = [%s]", NameOfField, i_get(NameOfField));
  }
  else
  {
    DIAG(DIAG_HIGH, "iGet(): %-30s = <Does not exist>", NameOfField);
  }
  return i_get(NameOfField);
}

/* -----------------------------------------------------------------------------
* Function  : format_date()
* Purpose   : Change date format from an input to an output
* Arguments : src - input date, pattern - input format, target - char pointer to store output,  targetPattern - target format
* Return    : 0 if successful
* --------------------------------------------------------------------------- */
int format_date(char* src, char* pattern, char* target, const char* targetPattern, int targetLenth, int secondsToAdd)
{   
    if(strlen(src)==0)
    {
      DIAG(DIAG_HIGH, "format_date Failed. Empty source string.");
 
      return -2;
    }
    struct tm result;
	
    memset(&result, 0, sizeof(struct tm));
    if (strptime(src, pattern,&result) == NULL)
    {
          DIAG(DIAG_HIGH, "\nstrptime failed\n");
          DIAG(DIAG_HIGH, "'%s'\t'%s' '%s'\t'%s' '%s'\t'%s'\t'%s'\n",  "Failed. format_date returning", "source",src,
                                                                        "srcPattern",pattern,"target",target);

          target = NULL;
          return 1;
    }
    else
    {
          DIAG(DIAG_HIGH,"tm_hour:  %d\n",result.tm_hour);
          DIAG(DIAG_HIGH,"tm_min:  %d\n",result.tm_min);
          DIAG(DIAG_HIGH,"tm_sec:  %d\n",result.tm_sec);
          DIAG(DIAG_HIGH,"tm_mon:  %d\n",result.tm_mon);
          DIAG(DIAG_HIGH,"tm_mday:  %d\n",result.tm_mday);
          DIAG(DIAG_HIGH,"tm_year:  %d\n",result.tm_year);
          DIAG(DIAG_HIGH,"tm_yday:  %d\n",result.tm_yday);
          DIAG(DIAG_HIGH,"tm_wday:  %d\n",result.tm_wday);
		  if(secondsToAdd > 0)
		  {
			result.tm_sec += secondsToAdd;
            timegm(&result);
		  }
          strftime(target,targetLenth,targetPattern , &result);
          
          DIAG(DIAG_HIGH, "'%s'\t'%s' '%s'\t'%s' '%s'\t'%s'\t'%s'\n",  "Success. format_date returning", "source",
                                                                  src, "targetPattern",targetPattern,"target",target);
          return 0;
    }
}

/* -----------------------------------------------------------------------------
* Function  : process_eoIUPS()
* Purpose   : process eoIUPS record
* Arguments : 
* Return    : PASS - FAIL
* --------------------------------------------------------------------------- */
int process_eoIUPS()
{
  DIAG(DIAG_HIGH, "process_eoIUPS() Started Processing");

  char el_imsi[100]={'\0'};
  char el_imei[100]={'\0'};
  char in_sac[100]={'\0'};
  char* el_sac = NULL;
  char in_lac[100]={'\0'};

  audit_out_type("eoIUPS-TDR-9.0");
  
  if(process_date("START_DATE_AND_TIME","EL_Start_of_Connection", "%d/%m/%Y %H:%M:%S") == FAIL)
  {
    return FAIL;
  }
  else
  {
    char out_time[25]= {'\0'};
    if(strlen(iGet("START_DATE_AND_TIME")) == 13)
    {
      sprintf(out_time,"%s:%s", o_get("EL_Start_of_Connection"), iGet("START_DATE_AND_TIME")+10);
      o_set_field("EL_Start_of_Connection", out_time);
    }
  }

  
  
  if(strlen(iGet("IMSI")) > 0)
  {
    sprintf(el_imsi,"·%s", iGet("IMSI"));
    o_add_field("EL_Imsi", el_imsi);
  }
  if(strlen(iGet("IMEI")) > 0)
  {
    sprintf(el_imei,"·%s", iGet("IMEI"));
    o_add_field("EL_Imei", el_imei);
  }

  strcpy(in_sac, iGet("SAC_3"));
  el_sac = strrchr(in_sac, '-');
  DIAG(DIAG_HIGH, "process_eoIUPS() input SAC_3 [%s]", in_sac);
  if(el_sac != NULL)
  {
    o_add_field("EL_SAC_value", el_sac+1);
  }

  if(strcmp("1", iGet("Attach_Attempt")) == 0)
  {
    o_add_field("EL_First_event", "GMM_AT_REQ");
  }

  if(strcmp("1", iGet("Attach_Success")) == 0)
  {
    o_add_field("EL_Last_event", "GMM_AT_CMP");
  }
  else if(strcmp("1", iGet("Detach_Success")) == 0)
  {
    o_add_field("EL_Last_event", "IMSID_Phx");
  }
  else if(strcmp("1", iGet("Activate_PDP_Context_Reject")) == 0)
  {
    o_add_field("EL_Last_event", "SM_A_C_Rej");
  }

  o_add_field("EL_Cause", get_el_IUPS_cause(atoi( iGet("PDP_ACT_FAIL_CAUSE") )) );
  
  strcpy(in_lac, iGet("LAC_3"));
  DIAG(DIAG_HIGH, "process_eoIUPS() input LAC_3 [%s]", in_lac);
  extract_add_field(in_lac, ": ", 2, "EL_SAI_LAC");
  
  DIAG(DIAG_HIGH, "process_eoIUPS() Finished Processing. Returning PASS.");

  return PASS;
}

/* -----------------------------------------------------------------------------
* Function  : process_eoIUCS()
* Purpose   : process eoIUCS record
* Arguments : 
* Return    : PASS - FAIL
* --------------------------------------------------------------------------- */
int process_eoIUCS()
{
  DIAG(DIAG_HIGH, "process_eoIUCS() Started Processing");
  char el_imsi[100]={'\0'};
  char el_imei[100]={'\0'};
  char in_sac[100]={'\0'};
  char* el_sac = NULL;
  char in_lac[100]={'\0'};

  audit_out_type("eoIUCS-TDR-9.0");
  
  if(process_date("START_DATE_AND_TIME","EL_Start_of_Connection", "%d/%m/%Y %H:%M:%S") == FAIL)
  {
    return FAIL;
  }
  else
  {
    char out_time[25]= {'\0'};
    if(strlen(iGet("START_DATE_AND_TIME")) == 13)
    {
      sprintf(out_time,"%s:%s", o_get("EL_Start_of_Connection"), iGet("START_DATE_AND_TIME")+10);
      o_set_field("EL_Start_of_Connection", out_time);
    }
  }

  if(strlen(iGet("IMSI")) > 0)
  {
    sprintf(el_imsi,"·%s", iGet("IMSI"));
    o_add_field("EL_Imsi", el_imsi);
  }
  if(strlen(iGet("IMEI")) > 0)
  {
    sprintf(el_imei,"·%s", iGet("IMEI"));
    o_add_field("EL_Imei", el_imei);
  }

  if(strcmp(iGet("IU_RELEASE_CAUSE"), "46") ==0)
  {
    o_add_field("EL_Cause", "RaConWithUELost");
  }

  o_add_field("EL_First_event", get_data_rec_type( atoi(iGet("DataRecordType")) ));

  if(strcmp("1", iGet("Location_Update_Success")) == 0)  
  {
    o_add_field("EL_Last_event", "LUACC");
  }
  else if(strcmp("1", iGet("IMSI_Detach_Indication")) == 0)
  {
    o_add_field("EL_Last_event", "IMSID");
  }
  else if(strcmp("46", iGet("IU_RELEASE_CAUSE")) == 0)
  {
    o_add_field("EL_Last_event", "IuRELREQ_RAD");
  }
  else if(strcmp("1", iGet("Service_Reject")) == 0)
  {
    o_add_field("EL_Last_event", "CMSRJ");
  }
  
  strcpy(in_sac, iGet("SAC_3"));
  el_sac = strrchr(in_sac, '-');
  DIAG(DIAG_HIGH, "process_eoIUCS() input SAC_3 [%s]", in_sac);
  if(el_sac != NULL)
  {
    o_add_field("EL_SAC_value", el_sac+1);
  }
  
  strcpy(in_lac, iGet("LAC_3"));
  DIAG(DIAG_HIGH, "process_eoIUCS() input LAC_3 [%s]", in_lac);
  extract_add_field(in_lac, ": ", 2, "EL_SAI_LAC");
  
  DIAG(DIAG_HIGH, "process_eoIUCS() Finished Processing. Returning PASS.");
  return PASS;
}

/* -----------------------------------------------------------------------------
* Function  : process_eoAINT()
* Purpose   : process eoAINT record
* Arguments : 
* Return    : PASS - FAIL
* --------------------------------------------------------------------------- */
int process_eoAINT()
{
  DIAG(DIAG_HIGH, "process_eoAINT() Started Processing");
  char el_imsi[100]={'\0'};
  char el_imei[100]={'\0'};
  char in_lac[100]={'\0'};
  char in_last_cell[100]={'\0'};
  /*char* el_lac=NULL;*/

  audit_out_type("eoAINT-TDR-7.0");
  if(process_date("START_DATE_AND_TIME","EL_Start_of_Connection", "%d/%m/%Y") == FAIL)
  {
    return FAIL;
  }
  if(process_date("START_DATE_AND_TIME","EL_START_of_Connection_time", "%H:%M:%S") == FAIL)
  {
    return FAIL;
  }
  else
  {
    char out_time[15]= {'\0'};
    if(strlen(iGet("START_DATE_AND_TIME")) == 13)
    {
      sprintf(out_time,"%s:%s", o_get("EL_START_of_Connection_time"), iGet("START_DATE_AND_TIME")+10);
      o_set_field("EL_START_of_Connection_time", out_time);
    }
  }
  /*MCC MNC rules*/
  if(strlen(iGet("IMSI")) > 0)
  {
    sprintf(el_imsi,"·%s", iGet("IMSI"));
    o_add_field("EL_Imsi", el_imsi);
    if(strncmp("604", iGet("IMSI"), 3) == 0)
    {
      o_add_field("MCC", "604");
    }
    if(strncmp("00", iGet("IMSI")+3, 2) == 0)
    {
      o_add_field("MNC", "00");
    }
  }
  if(strlen(iGet("IMEI")) > 0)
  {
    sprintf(el_imei,"·%s", iGet("IMEI"));
    o_add_field("EL_Imei", el_imei);
  }

  strcpy(in_lac, iGet("LAC_3"));
  /*el_lac = strstr(in_lac, ":");
  DIAG(DIAG_HIGH, "process_eoAINT() input LAC [%s]", in_lac);
  if(el_lac != NULL)
  {
    o_add_field("EL_Lac", el_lac+1);
  }
  else
  {
    o_add_field("EL_Lac", in_lac);
  }*/
  extract_add_field(in_lac, ": ", 2, "EL_Lac");

  o_add_field("EL_Cause", get_el_cause(atoi( iGet("BSSMAP_CLEAR_CAUSE") )) );
  o_add_field("EL_Start_event", get_AINT_start_event(atoi( iGet("CM_SERV_REQ_TYPE") )) );
  
  if(strcmp("1", iGet("LOCATION_UPDATE_SUCCESS")) == 0)  
  {
    o_add_field("EL_End_event", "LUACC");
  }
  else if(strcmp("1", iGet("IMSI_DETACH_STATUS")) == 0)
  {
    o_add_field("EL_End_event", "IMSID_Phx");
  }
  else if(strcmp("1", iGet("SERVICE_REJECT")) == 0)
  {
    o_add_field("EL_End_event", "CMSRJ");
  }
  
  char* el_cell_p = NULL;
  char el_cell_id[100] = {'\0'};
  strcpy(in_last_cell, iGet("LAST_CELL"));
  DIAG(DIAG_HIGH, "process_eoAINT() input LAST_CELL [%s]", in_last_cell);
  el_cell_p = strstr(in_last_cell, ":");
  if(el_cell_p != NULL)
  {
	char tmp_lac_ci[100] = {'\0'};
	while(*(el_cell_p) != '\0')
	{
	  char c = *(el_cell_p);
	  if(c >= '0' && c <= '9')
	  {
	  	strncat(tmp_lac_ci, &c, 1);
	  }
	  else if(c == '-')
	  {
	  	c = '_';
	  	strncat(tmp_lac_ci, &c, 1);
	  }
	  el_cell_p++;
	}
	sprintf(el_cell_id, "A_B_H_%s", tmp_lac_ci);
    o_add_field("EL_Cell_ID", el_cell_id);
  }
  
  DIAG(DIAG_HIGH, "process_eoAINT() input DATARECORDTYPE [%s]", iGet("DATARECORDTYPE"));
  if(strcmp("5", iGet("DATARECORDTYPE")) == 0)
  {
    o_add_field("EL_Previous_State", "LU_IA");
  }
  
  DIAG(DIAG_HIGH, "process_eoAINT() Finished Processing. Returning PASS.");
  return PASS;
}

/* -----------------------------------------------------------------------------
* Function  : process_eoGB()
* Purpose   : process eoGB record
* Arguments : 
* Return    : PASS - FAIL
* --------------------------------------------------------------------------- */
int process_eoGB()
{
  DIAG(DIAG_HIGH, "process_eoGB() Started Processing.");
  char el_imsi[100]={'\0'};
  char el_imei[100]={'\0'};
  char in_lac[100]={'\0'};

  audit_out_type("eoGB-TDR-6.0");
  if(process_date("START_DATE_AND_TIME","EL_Start_of_Connection", "%d/%m/%Y") == FAIL)
  {
    return FAIL;
  }
  if(process_date("START_DATE_AND_TIME","EL_START_of_Connection_time", "%H:%M:%S") == FAIL)
  {
    return FAIL;
  }
  else
  {
    char out_time[15]= {'\0'};
    if(strlen(iGet("START_DATE_AND_TIME")) == 13)
    {
      sprintf(out_time,"%s:%s", o_get("EL_START_of_Connection_time"), iGet("START_DATE_AND_TIME")+10);
      o_set_field("EL_START_of_Connection_time", out_time);
    }
  }
  /*if(process_date("START_DATE_AND_TIME","EL_Start_of_Connection", "%d/%m/%Y %H:%M:%S") == FAIL)
  {
    return FAIL;
  }
  else
  {
    char out_time[25]= {'\0'};
    if(strlen(iGet("START_DATE_AND_TIME")) == 13)
    {
      sprintf(out_time,"%s:%s", o_get("EL_Start_of_Connection"), iGet("START_DATE_AND_TIME")+10);
      o_set_field("EL_Start_of_Connection", out_time);
      o_add_field("EL_Start_msec", iGet("START_DATE_AND_TIME")+10);
    }
  }*/
  if(strlen(iGet("IMSI")) > 0)
  {
    sprintf(el_imsi,"·%s", iGet("IMSI"));
    o_add_field("EL_Imsi", el_imsi);
  }
  if(strlen(iGet("IMEI")) > 0)
  {
    sprintf(el_imei,"·%s", iGet("IMEI"));
    o_add_field("EL_Imei", el_imei);
  }

  o_add_field("EL_Type", get_eoGB_rec_type( atoi(iGet("DATA_RECORD_TYPE")) ));

  strcpy(in_lac, iGet("LAC"));
  extract_add_field(in_lac, ": ", 2, "EL_LAC");
  
  extract_add_field(iGet("CELL_IDENTITY"), ": ", 2, "EL_Cell_Id");

  if(strcmp("1", iGet("ACT_PDP_CTXT_REJ")) == 0)
  {
    o_add_field("EL_Event", "SM_A_C_Rej");
  }

  o_add_field("EL_Cause", get_el_IUPS_cause(atoi( iGet("PDP_ACT_FAIL_CAUSE") )) );

  DIAG(DIAG_HIGH, "process_eoGB() Finished Processing. Returning PASS.");
  return PASS;
}

/* -----------------------------------------------------------------------------
* Function  : process_eoMAP()
* Purpose   : process eoMAP record
* Arguments : 
* Return    : PASS - FAIL
* --------------------------------------------------------------------------- */
int process_eoMAP()
{
  DIAG(DIAG_HIGH, "process_eoMAP() Started Processing.");
  char el_imsi[100]={'\0'};
  char el_imei[100]={'\0'};

  audit_out_type("eoMAP-TDR-5.0");
  if(process_date("START_DATE_AND_TIME","EL_Start_of_Connection", "%d/%m/%Y") == FAIL)
  {
    return FAIL;
  }
  if(process_date("START_DATE_AND_TIME","EL_START_of_Connection_time", "%H:%M:%S") == FAIL)
  {
    return FAIL;
  }
  else
  {
    char out_time[15]= {'\0'};
    if(strlen(iGet("START_DATE_AND_TIME")) == 13)
    {
      sprintf(out_time,"%s:%s", o_get("EL_START_of_Connection_time"), iGet("START_DATE_AND_TIME")+10);
      o_set_field("EL_START_of_Connection_time", out_time);
    }
  }
  
  if(strlen(iGet("IMSI")) > 0)
  {
    sprintf(el_imsi,"·%s", iGet("IMSI"));
    o_add_field("EL_Imsi", el_imsi);
  }
  if(strlen(iGet("IMEI")) > 0)
  {
    sprintf(el_imei,"·%s", iGet("IMEI"));
    o_add_field("EL_Imei", el_imei);
  }
  
  if(strcmp("2", iGet("OP_CODE")) == 0)
  {
    o_add_field("EL_Start_Event", "MAP_UPDATE_LOCATION");
  }
  else if(strcmp("23", iGet("OP_CODE")) == 0)
  {
    o_add_field("EL_Start_Event", "MAP_UPDATE_GPRS_LOCATION");
  }
  else if(strcmp("67", iGet("MAP_FL_OP_CODE")) == 0)
  {
    o_add_field("EL_Start_Event", "MAP_PURGE_MS");
  }
  else
  {
    return FAIL;
  }

  if(strcmp("opercountry1", iGet("NET_OPERATOR_NAME")) == 0)
  {
    o_add_field("EL_LOCATION_OPERATOR", "opercountry1");
  }
  if(strlen(iGet("NET_OPERATOR_NAME_FIRST_PLMN")) == 5 || strlen(iGet("NET_OPERATOR_NAME_FIRST_PLMN")) == 6)
  {
    char el_mcc[10]={'\0'};
    strncpy(el_mcc, iGet("NET_OPERATOR_NAME_FIRST_PLMN"), 3);
    o_add_field("MCC", el_mcc);
    char el_mnc[10]={'\0'};
    strcpy(el_mnc, iGet("NET_OPERATOR_NAME_FIRST_PLMN")+3);
    o_add_field("MNC", el_mnc);
  }

  DIAG(DIAG_HIGH, "process_eoMAP() Finished Processing. Returning PASS.");
  return PASS;
}

/* -----------------------------------------------------------------------------
* Function  : process_eoLTE()
* Purpose   : process eoLTE record
* Arguments : 
* Return    : PASS - FAIL
* --------------------------------------------------------------------------- */
int process_eoLTE()
{
  DIAG(DIAG_HIGH, "process_eoLTE() Started Processing.");
  char el_imsi[100]={'\0'};
  char el_imei[100]={'\0'};

  audit_out_type("eoLTE-FULL-csv-export-eoLTE-5.0");

  if(strcmp(iGet("INTERFACE_TYPE"), "1") != 0)
  {
    return FAIL;
  }
  
  
  if(strlen(iGet("IMSI")) > 0)
  {
    sprintf(el_imsi,"·%s", iGet("IMSI"));
    o_add_field("EL_Imsi", el_imsi);
  }
  if(strlen(iGet("IMEISV")) > 0)
  {
    snprintf(el_imei, strlen(iGet("IMEISV"))+1, "·%s", iGet("IMEISV"));
    o_add_field("EL_Imei", el_imei);
  }

  if(strcmp("1", iGet("S1_ATTACH_ATT")) == 0)
  {
    o_add_field("EL_Start_EMM_Event", "Attach Req");
  }
  else if(strcmp("1", iGet("S1_DETACH_REQUEST")) == 0)
  {
    o_add_field("EL_Start_EMM_Event", "Detach Req");
  }
  
  if(strcmp("1", iGet("S1_PDN_CONNECTIVITY_REJECT_CAUSE")) == 0)
  {
    o_add_field("EL_Start_ESM_Event", "PDN CONNECTIVITY REJECT");
  }
  
  o_add_field("EL_enodeb_id", iGet("GLOBAL_ENB_ID"));

  extract_add_field(iGet("E_CGI"), ": ", 2, "EL_cell_id");

  if(strcmp("1", iGet("S1_ATTACH_SUCC")) == 0)
  {
    o_add_field("EL_Last_EMM_Event", "Attach Cmp");
  }

  DIAG(DIAG_HIGH, "process_eoLTE() Finished Processing. Returning PASS.");
  return PASS;
}

/* -----------------------------------------------------------------------------
* Function  : get_el_cause()
* Purpose   : Get eoAINT mapped string for EL_CAUSE
* Arguments : int cause - the value of incoming cause
* Return    : Mapped string
* --------------------------------------------------------------------------- */
char* get_el_cause(int cause)
{
  DIAG(DIAG_HIGH, "get_el_cause() input [%d]", cause);
  switch(cause)
  {
    case 89: return "E///SlcFR";
    case 90: return "E///SlcHR";
    case 91: return "E///SlcOLFR";
    case 92: return "E///SlcOLHR";
    default: return "";
  }   
}

/* -----------------------------------------------------------------------------
* Function  : get_el_IUPS_cause()
* Purpose   : Get eoIUPS/eoGB mapped string for EL_CAUSE
* Arguments : int cause - the value of incoming cause
* Return    : Mapped string
* --------------------------------------------------------------------------- */
char* get_el_IUPS_cause(int cause)
{
  DIAG(DIAG_HIGH, "get_el_IUPS_cause() input [%d]", cause);
  switch(cause)
  {
    case 8: return "SM_OperDetBarr";
    case 26: return "SM_InsRes";
    case 27: return "SM_MissUnApn";
    case 28: return "SM_UnPdpAddTyp";
    case 29: return "SM_UAutFail";
    case 30: return "SM_ActRejGgsn";
    case 21: return "SM_ActRejUnsp";
    case 32: return "SM_SOptNS";
    case 33: return "SM_ReqSONS";
    case 34: return "SM_SOptTempOut";
    case 35: return "SM_NSapiUsed";
    case 111: return "SM_PErrUnsp";
    default: return "";
  }   
}

/* -----------------------------------------------------------------------------
* Function  : get_AINT_start_event()
* Purpose   : Get eoAINT mapped string for start event
* Arguments : int serv_type - the value of incoming type
* Return    : Mapped string
* --------------------------------------------------------------------------- */
char* get_AINT_start_event(int serv_type)
{
  DIAG(DIAG_HIGH, "get_AINT_start_event() input [%d]", serv_type);
  switch(serv_type)
  {
    case 1: return "CMSRQ_NC_Phx";
    case 4: return "CMSRQ_SMS_Phx";
    case 8: return "CMSRQ_SS_Phx";
    case 2: return "CMSRQ_EC_Phx";
    default: return "";
  } 
}

/* -----------------------------------------------------------------------------
* Function  : get_data_rec_type()
* Purpose   : Get eoIUCS mapped string for data rec type
* Arguments : int rec_type - the value of incoming type
* Return    : Mapped string
* --------------------------------------------------------------------------- */
char* get_data_rec_type(int rec_type)
{
  DIAG(DIAG_HIGH, "get_data_rec_type() input [%d]", rec_type);
  switch(rec_type)
  {
    case 9: return "LUREQ_IA_Phx";
    case 1: return "CMSRQ_NC_Phx";
    case 12: return "CMSRQ_EC_Phx";
    case 10: return "CMSRQ_SS_Phx";
    case 3: return "CMSRQ_SMS_Phx";
    default: return "";
  } 
}

/* -----------------------------------------------------------------------------
* Function  : get_eoGB_rec_type()
* Purpose   : Get eoGB mapped string for data rec type
* Arguments : int rec_type - the value of incoming type
* Return    : Mapped string
* --------------------------------------------------------------------------- */
char* get_eoGB_rec_type(int rec_type)
{
  DIAG(DIAG_HIGH, "get_eoGB_rec_type() input [%d]", rec_type);
  switch(rec_type)
  {
    case 1: return "1";
    case 4: return "17";
    case 2: return "24";
    case 3: return "26";
    default: return "";
  } 
}

/* -----------------------------------------------------------------------------
* Function  : extract_add_field()
* Purpose   : Extract substring after the first occurence of a separator and add field to output record
* Arguments : field_val: input field value - sep: separator - sep_sz: length of sep string - out_field: name of output field
* Return    : void
* --------------------------------------------------------------------------- */
void extract_add_field(const char* field_val, const char* sep, int sep_sz, const char* out_field)
{
  char* sep_str;
  sep_str = strstr(field_val, sep);
  DIAG(DIAG_HIGH, "extract_add_field() input [%s]", field_val);
  if(sep_str != NULL)
  {
    o_add_field(out_field, sep_str+sep_sz);
  }
  else
  {
    o_add_field(out_field, field_val);
  }
}