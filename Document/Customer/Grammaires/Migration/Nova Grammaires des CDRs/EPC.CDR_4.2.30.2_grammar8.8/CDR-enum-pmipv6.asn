CDR-enum-pmipv6 { version (0) cdr-enum-pmipv6 (5) }

DEFINITIONS AUTOMATIC TAGS ::=

BEGIN

    -- DATA-REF-BEGIN : S2a-procedure-initiated-by
    S2a-procedure-initiated-by ::= ENUMERATED
    {
        s2a-procedure-initiated-by-MAG    (0),
        s2a-procedure-initiated-by-LMA    (1),
        ...
    }
    -- DATA-REF-END

    -- DATA-REF-BEGIN : S2a-binding-revocation-trigger
    S2a-binding-revocation-trigger ::= ENUMERATED
    {
        s2a-binding-revocation-trigger-unspecified                                 (0),
        s2a-binding-revocation-trigger-administrative-reason                       (1),
        s2a-binding-revocation-trigger-inter-mag-handover-same-access-type         (2),
        s2a-binding-revocation-trigger-inter-mag-handover-different-access-type    (3),
        s2a-binding-revocation-trigger-inter-mag-handover-unknown                  (4),
        s2a-binding-revocation-trigger-user-initiated-session-termination          (5),
        s2a-binding-revocation-trigger-access-network-session-termination          (6),
        s2a-binding-revocation-trigger-possible-out-of-sync-bce-state              (7),
        s2a-binding-revocation-trigger-per-peer-policy                             (128),
        s2a-binding-revocation-trigger-revoking-mobility-node-local-policy         (129),
        s2a-binding-revocation-trigger-reserved-for-testing-purpose-250            (250),
        s2a-binding-revocation-trigger-reserved-for-testing-purpose-251            (251),
        s2a-binding-revocation-trigger-reserved-for-testing-purpose-252            (252),
        s2a-binding-revocation-trigger-reserved-for-testing-purpose-253            (253),
        s2a-binding-revocation-trigger-reserved-for-testing-purpose-254            (254),
        s2a-binding-revocation-trigger-reserved-for-testing-purpose-255            (255),
        ...
    }
    -- DATA-REF-END

    -- DATA-REF-BEGIN : S2a-pmipv6-handoff-indicator
    S2a-pmipv6-handoff-indicator ::= ENUMERATED
    {
        s2a-pmipv6-handoff-indicator-reserved                                      (0),
        s2a-pmipv6-handoff-indicator-attachment-over-a-new-interface               (1),
        s2a-pmipv6-handoff-indicator-handoff-between-two-different-interfaces      (2),
        s2a-pmipv6-handoff-indicator-handoff-between-mag-for-the-same-interface    (3),
        s2a-pmipv6-handoff-indicator-handoff-state-unknown                         (4),
        s2a-pmipv6-handoff-indicator-handoff-state-not-changed-re-registration     (5),
        ...
    }
    -- DATA-REF-END

    -- Type de cause sur l'interface S2a
    -- DATA-REF-BEGIN : S2a-root-cause
    S2a-root-cause ::= ENUMERATED
    {
        s2a-root-cause-no-answer                 (0),
        s2a-root-cause-limited-success           (1),
        s2a-root-cause-core-internal-failures    (2),
        s2a-root-cause-misc                      (3),
        s2a-root-cause-user-failures             (4),
        s2a-root-cause-protocol-errors           (5),
        s2a-root-cause-unknown-apn               (6),
        ...
    }
    -- DATA-REF-END

    -- PMIPv6 message type sur l'interface S2a
    -- DATA-REF-BEGIN : PMIPv6-message
    PMIPv6-message ::= ENUMERATED
    {
        pmipv6-binding-refresh-request              (0),
        pmipv6-home-test-init                       (1),
        pmipv6-care-of-test-init                    (2),
        pmipv6-home-test                            (3),
        pmipv6-care-of-test                         (4),
        pmipv6-binding-update                       (5),
        pmipv6-binding-acknowledge                  (6),
        pmipv6-binding-error                        (7),
        pmipv6-fast-binding-update                  (8),
        pmipv6-fast-binding-acknowledgment          (9),
        pmipv6-fast-neighbor-advertisement          (10),
        pmipv6-experimental-mobility-header         (11),
        pmipv6-home-agent-switch-message            (12),
        pmipv6-heartbeat-message                    (13),
        pmipv6-handover-initiate-message            (14),
        pmipv6-handover-acknowledge-message         (15),
        pmipv6-binding-revocation-message           (16),
        pmipv6-localized-routing-initiation         (17),
        pmipv6-localized-routing-acknowledgement    (18),
        ...
    }
    -- DATA-REF-END

    -- Dernier etat du CDR dans le cas du protocole PMIPv6 sur l'interface S2a
    -- DATA-REF-BEGIN : PMIPv6-last-state
    PMIPv6-last-state ::= ENUMERATED
    {
        pmipv6-last-state-wait-proxy-binding-acknowledgment          (0),
        pmipv6-last-state-leave-proxy-binding-acknowledgment         (1),
        pmipv6-last-state-wait-binding-revocation-acknowledgment     (2),
        pmipv6-last-state-leave-binding-revocation-acknowledgment    (3),
        pmipv6-last-state-wait-heartbeat-reply                       (4),
        pmipv6-last-state-leave-heartbeat-reply                      (5),
        ...
    }
    -- DATA-REF-END

    -- Cause protocolaire du message Proxy Binding Acknowledgement sur l'interface S2a
    -- DATA-REF-BEGIN : PMIPv6-status-pba
    PMIPv6-status-pba ::= ENUMERATED
    {
        pmipv6-status-pba-binding-update-accepted-proxy-binding-update-accepted    (0),
        pmipv6-status-pba-accepted-but-prefix-discovery-necessary                  (1),
        pmipv6-status-pba-gre-key-option-not-required                              (2),
        pmipv6-status-pba-gre-tunneling-but-tlv-header-not-supported               (3),
        pmipv6-status-pba-mcoa-notcomplete                                         (4),
        pmipv6-status-pba-mcoa-returnhome-wo-ndp                                   (5),
        pmipv6-status-pba-pbu-accepted-tb-ignored-settingsmismatch                 (6),
        pmipv6-status-pba-reason-unspecified                                       (128),
        pmipv6-status-pba-administratively-prohibited                              (129),
        pmipv6-status-pba-insufficient-resources                                   (130),
        pmipv6-status-pba-home-registration-not-supported                          (131),
        pmipv6-status-pba-not-home-subnet                                          (132),
        pmipv6-status-pba-not-home-agent-for-this-mobile-node                      (133),
        pmipv6-status-pba-duplicate-address-detection-failed                       (134),
        pmipv6-status-pba-sequence-number-out-of-window                            (135),
        pmipv6-status-pba-expired-home-nonce-index                                 (136),
        pmipv6-status-pba-expired-care-of-nonce-index                              (137),
        pmipv6-status-pba-expired-nonces                                           (138),
        pmipv6-status-pba-registration-type-change-disallowed                      (139),
        pmipv6-status-pba-mobile-router-operation-not-permitted                    (140),
        pmipv6-status-pba-invalid-prefix                                           (141),
        pmipv6-status-pba-not-authorized-for-prefix                                (142),
        pmipv6-status-pba-forwarding-setup-failed                                  (143),
        pmipv6-status-pba-mipv6-id-mismatch                                        (144),
        pmipv6-status-pba-mipv6-mesg-id-reqd                                       (145),
        pmipv6-status-pba-mipv6-auth-fail                                          (146),
        pmipv6-status-pba-permanent-home-keygen-token-unavailable                  (147),
        pmipv6-status-pba-cga-and-signature-verification-failed                    (148),
        pmipv6-status-pba-permanent-home-keygen-token-exists                       (149),
        pmipv6-status-pba-non-null-home-nonce-index-expected                       (150),
        pmipv6-status-pba-service-authorization-failed                             (151),
        pmipv6-status-pba-proxy-reg-not-enabled                                    (152),
        pmipv6-status-pba-not-lma-for-this-mobile-node                             (153),
        pmipv6-status-pba-mag-not-authorized-for-proxy-reg                         (154),
        pmipv6-status-pba-not-authorized-for-home-network-prefix                   (155),
        pmipv6-status-pba-timestamp-mismatch                                       (156),
        pmipv6-status-pba-timestamp-lower-than-prev-accepted                       (157),
        pmipv6-status-pba-missing-home-network-prefix-option                       (158),
        pmipv6-status-pba-bce-pbu-prefix-set-do-not-match                          (159),
        pmipv6-status-pba-missing-mn-identifier-option                             (160),
        pmipv6-status-pba-missing-handoff-indicator-option                         (161),
        pmipv6-status-pba-missing-access-tech-type-option                          (162),
        pmipv6-status-pba-gre-key-option-required                                  (163),
        pmipv6-status-pba-mcoa-malformed                                           (164),
        pmipv6-status-pba-mcoa-non-mcoa-binding-exists                             (165),
        pmipv6-status-pba-mcoa-prohibited                                          (166),
        pmipv6-status-pba-mcoa-unknown-coa                                         (167),
        pmipv6-status-pba-mcoa-bulk-registration-prohibited                        (168),
        pmipv6-status-pba-mcoa-simultaneous-home-and-foreign-prohibited            (169),
        pmipv6-status-pba-not-authorized-for-ipv4-mobility-service                 (170),
        pmipv6-status-pba-not-authorized-for-ipv4-home-address                     (171),
        pmipv6-status-pba-not-authorized-for-ipv6-mobility-service                 (172),
        pmipv6-status-pba-multiple-ipv4-home-address-assignment-not-supported      (173),
        pmipv6-status-pba-invalid-care-of-address                                  (174),
        pmipv6-status-pba-invalid-mobile-node-group-identifier                     (175),
        pmipv6-status-pba-reinit-sa-with-hac                                       (176),
        ...
    }
    -- DATA-REF-END

    -- Cause protocolaire du message Binding Revocation Acknowledgement sur l'interface S2a
    -- DATA-REF-BEGIN : PMIPv6-status-bra
    PMIPv6-status-bra ::= ENUMERATED
    {
        pmipv6-status-bra-success                                   (0),
        pmipv6-status-bra-partial-success                           (1),
        pmipv6-status-bra-binding-does-not-exist                    (128),
        pmipv6-status-bra-ipv4-home-address-option-required         (129),
        pmipv6-status-bra-global-revocation-not-authorized          (130),
        pmipv6-status-bra-revoked-mobile-nodes-identity-required    (131),
        pmipv6-status-bra-revocation-failed-mn-is-attached          (132),
        pmipv6-status-bra-revocation-trigger-not-supported          (133),
        pmipv6-status-bra-revocation-function-not-supported         (134),
        pmipv6-status-bra-proxy-binding-revocation-not-supported    (135),
        ...
    }
    -- DATA-REF-END

END
