CDR-enum-radius { version (0) cdr-enum-radius (8) }

DEFINITIONS AUTOMATIC TAGS ::=

BEGIN

    -- DATA-REF-BEGIN : RADIUS-message
    RADIUS-message ::= ENUMERATED
    {
        radius-access-request                          (1),
        radius-access-accept                           (2),
        radius-access-reject                           (3),
        radius-accounting-request                      (4),
        radius-accounting-response                     (5),
        radius-accounting-status-interim-accounting    (6),
        radius-password-request                        (7),
        radius-password-ack                            (8),
        radius-password-reject                         (9),
        radius-accounting-message                      (10),
        radius-access-challenge                        (11),
        radius-status-server                           (12),
        radius-status-client                           (13),
        radius-resource-free-request                   (21),
        radius-resource-free-response                  (22),
        radius-resource-query-request                  (23),
        radius-resource-query-response                 (24),
        radius-alternate-resource-reclaim-request      (25),
        radius-nas-reboot-request                      (26),
        radius-nas-reboot-response                     (27),
        radius-reserved28                              (28),
        radius-next-passcode                           (29),
        radius-new-pin                                 (30),
        radius-terminate-session                       (31),
        radius-password-expired                        (32),
        radius-event-request                           (33),
        radius-event-response                          (34),
        radius-disconnect-request                      (40),
        radius-disconnect-ack                          (41),
        radius-disconnect-nak                          (42),
        radius-coa-request                             (43),
        radius-coa-ack                                 (44),
        radius-coa-nak                                 (45),
        radius-ip-address-allocate                     (50),
        radius-ip-address-release                      (51),
        radius-experimental-use250                     (250),
        radius-experimental-use251                     (251),
        radius-experimental-use252                     (252),
        radius-experimental-use253                     (253),
        radius-reserved254                             (254),
        radius-reserved255                             (255),
        ...
    }
    -- DATA-REF-END

    -- DATA-REF-BEGIN : RADIUS-last-state
    RADIUS-last-state ::= ENUMERATED
    {
        radius-last-state-idle                                        (0),
        radius-last-state-wait-access-accept                          (1),
        radius-last-state-leave-access-accept                         (2),
        radius-last-state-leave-access-reject                         (3),
        radius-last-state-leave-access-challenge                      (4),
        radius-last-state-wait-accounting-response-start              (5),
        radius-last-state-leave-accounting-response-start             (6),
        radius-last-state-wait-accounting-response-stop               (7),
        radius-last-state-leave-accounting-response-stop              (8),
        radius-last-state-wait-accounting-response-interim-update     (9),
        radius-last-state-leave-accounting-response-interim-update    (10),
        radius-last-state-wait-accounting-response-on                 (11),
        radius-last-state-leave-accounting-response-on                (12),
        radius-last-state-wait-accounting-response-off                (13),
        radius-last-state-leave-accounting-response-off               (14),
        radius-last-state-wait-disconnect-ack                         (15),
        radius-last-state-leave-disconnect-ack                        (16),
        radius-last-state-leave-disconnect-nak                        (17),
        ...
    }
    -- DATA-REF-END

    -- DATA-REF-BEGIN : RADIUS-root-cause
    RADIUS-root-cause ::= ENUMERATED
    {
        radius-root-cause-no-answer                 (0),
        radius-root-cause-limited-success           (1),
        radius-root-cause-core-internal-failures    (2),
        radius-root-cause-misc                      (3),
        radius-root-cause-user-failures             (4),
        radius-root-cause-protocol-errors           (5),
        radius-root-cause-unknown-apn               (6),
        ...
    }
    -- DATA-REF-END

    -- Indique la cause de demande de fin de session
    -- DATA-REF-BEGIN : RADIUS-acct-terminate-cause
    RADIUS-acct-terminate-cause ::= ENUMERATED
    {
        radius-acct-terminate-cause-user-request                      (1),
        radius-acct-terminate-cause-lost-carrier                      (2),
        radius-acct-terminate-cause-lost-service                      (3),
        radius-acct-terminate-cause-idle-timeout                      (4),
        radius-acct-terminate-cause-session-timeout                   (5),
        radius-acct-terminate-cause-admin-reset                       (6),
        radius-acct-terminate-cause-admin-reboot                      (7),
        radius-acct-terminate-cause-port-error                        (8),
        radius-acct-terminate-cause-nas-error                         (9),
        radius-acct-terminate-cause-nas-request                       (10),
        radius-acct-terminate-cause-nas-reboot                        (11),
        radius-acct-terminate-cause-port-unneeded                     (12),
        radius-acct-terminate-cause-port-preempted                    (13),
        radius-acct-terminate-cause-port-suspended                    (14),
        radius-acct-terminate-cause-service-unavailable               (15),
        radius-acct-terminate-cause-callback                          (16),
        radius-acct-terminate-cause-user-error                        (17),
        radius-acct-terminate-cause-host-request                      (18),
        radius-acct-terminate-cause-supplicant-restart                (19),
        radius-acct-terminate-cause-reauthentication-failure          (20),
        radius-acct-terminate-cause-port-reinitialized                (21),
        radius-acct-terminate-cause-port-administratively-disabled    (22),
        radius-acct-terminate-cause-lost-power                        (23),
        ...
    }
    -- DATA-REF-END

    -- DATA-REF-BEGIN : RADIUS-error-cause
    RADIUS-error-cause ::= ENUMERATED
    {
        radius-error-cause-residual-session-context-removed          (201),
        radius-error-cause-invalid-eap-packet-ignored                (202),
        radius-error-cause-unsupported-attribute                     (401),
        radius-error-caus-emissing-attribute                         (402),
        radius-error-cause-nas-identification-mismatch               (403),
        radius-error-cause-invalid-request                           (404),
        radius-error-cause-unsupported-service                       (405),
        radius-error-cause-unsupported-extension                     (406),
        radius-error-cause-invalid-attribute-value                   (407),
        radius-error-cause-administratively-prohibited               (501),
        radius-error-cause-request-not-routable-proxy                (502),
        radius-error-cause-session-context-not-found                 (503),
        radius-error-cause-session-context-not-removable             (504),
        radius-error-cause-other-proxy-processing-error              (505),
        radius-error-cause-resources-unavailable                     (506),
        radius-error-cause-request-initiated                         (507),
        radius-error-cause-multiple-session-selection-unsupported    (508),
        radius-error-cause-location-info-required                    (509),
        ...
    }
    -- DATA-REF-END

    -- DATA-REF-BEGIN : RADIUS-acct-status-type
    RADIUS-acct-status-type ::= ENUMERATED
    {
        radius-acct-status-type-start                 (1),
        radius-acct-status-type-stop                  (2),
        radius-acct-status-type-interim-update        (3),
        radius-acct-status-type-accounting-on         (7),
        radius-acct-status-type-accounting-off        (8),
        radius-acct-status-type-tunnel-start          (9),
        radius-acct-status-type-tunnel-stop           (10),
        radius-acct-status-type-tunnel-reject         (11),
        radius-acct-status-type-tunnel-link-start     (12),
        radius-acct-status-type-tunnel-link-stop      (13),
        radius-acct-status-type-tunnel-link-reject    (14),
        radius-acct-status-type-failed                (15),
        ...
    }
    -- DATA-REF-END

END
