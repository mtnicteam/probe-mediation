/***********************************************************************
*
* Included libraries
*
***********************************************************************/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>
#define __USE_XOPEN
#include <time.h>
#include <stdbool.h>
#include "nodebase.h"

#include "app_authbln.h"

/* Include if needed
  #include <math.h>
  #include <stdbool.h>
  #include <regex.h>
*/

/***********************************************************************
*
* Reserved functions
*
***********************************************************************/

/* This function is called in the beginning when the node is starting up.
*
* Arguments:
*   None.
* Return values:
*   None.
*/
void node_init(void)
{
  DIAG(DIAG_LOW, "node_init(): entered the function");

  DIAG(DIAG_LOW, "node_init(): returning...");
}

/* This function is called for every input record or for every input
* file, if input is binary.
*
* Arguments:
*   None.
* Return values:
*   None.
*/
void node_process(void)
{
  DIAG(DIAG_HIGH, "node_process(): entered the function");
  
  nb_new_record();
  o_copy_input();

  /*process record types*/
  if(strcmp(i_get_input_type(), "eoIUPS") == 0)
  {
    if(process_eoIUPS() == FAIL)
    {
      return;
    }
  }
  else if(strcmp(i_get_input_type(), "eoIUCS") == 0)
  {
    if(process_eoIUCS() == FAIL)
    {
      return;
    }
  }
  else if(strcmp(i_get_input_type(), "eoAINT") == 0)
  {
    if(process_eoAINT() == FAIL)
    {
      return;
    }
  }
  else if(strcmp(i_get_input_type(), "eoGB") == 0)
  {
    if(process_eoGB() == FAIL)
    {
      return;
    }
  }
  else if(strcmp(i_get_input_type(), "eoMAP") == 0)
  {
    if(process_eoMAP() == FAIL)
    {
      return;
    }
  }
  else if(strcmp(i_get_input_type(), "eoLTE") == 0)
  {
    if(process_eoLTE() == FAIL)
    {
      return;
    }
  }
  else if(strcmp(i_get_input_type(), "eoIPSDR") == 0)
  {
    if(process_eoIPSDR() == FAIL)
    {
      return;
    }
  }
  else if(strcmp(i_get_input_type(), "eoTCAP") == 0)
  {
    if(process_eoTCAP() == FAIL)
    {
      return;
    }
  }
  
  if(strcmp(i_get_input_type(), "eoLTE") == 0)
  {
    nb_write_record("S1_OUT");
  }
  else
  {
    char outlink_name[100] = {0};
    sprintf(outlink_name, "%s_OUT", i_get_input_type());
    nb_write_record(outlink_name);
  }
  
  

  DIAG(DIAG_MEDIUM, "node_process(): Sending output");  
}

/* This function is called whenever a control record is received.
*
* Arguments:
*   None.
* Return values:
*   None.
*/
void node_control(void)
{
  DIAG(DIAG_LOW, "node_control()");
}

/* This function is called when the node commits after processing an
* input file successfully.
*
* Arguments:
*   None.
* Return values:
*   None.
*/
void node_commit(void)
{
  DIAG(DIAG_LOW, "node_commit()");


  DIAG(DIAG_HIGH, "node_commit(): Leaving the function");
}

/* This function is called in the end when the node is shutting down.
*
* Arguments:
*   None.
* Return values:
*   None.
*/
void node_end(void)
{
  DIAG(DIAG_LOW, "node_end()");
}

/* This function is called when the operator requested the flushing
* of the steram. If the node stores any records in an internal
* storage, all records should be retrieved from the storage and
* written to the output in this function.
*
* Arguments:
*   None.
* Return values:
*   None.
*/
void node_flush(void)
{
  DIAG(DIAG_LOW, "node_flush()");
}


/* This function is called if an error occurs during the processing of a
* file/record, and should be used to reset the system to the point of 
* the last commit.
*
* Arguments:
*   None.
* Return values:
*   None.
*/
void node_rollback(void)
{
  DIAG(DIAG_LOW, "node_rollback()");
}

/* This function is called whenever the node is scheduled to be executed.
*
* Arguments:
*   None.
* Return values:
*   NB_OK if the scheduled functionality is executed successfully, NB_ERROR otherwise
*/
int node_schedule(void)
{
  DIAG(DIAG_LOW, "node_schedule(): entered the function");
  DIAG(DIAG_LOW, "node_schedule(): returning NB_OK...");
  return NB_OK;
}

/* This function is called in regular intervals, about every second.
*
* Arguments:
*   None.
* Return values:
*   None.
*/
void node_timer(void)
{
  DIAG(DIAG_HIGH, "node_timer()"); 
}

/* This function is called for real-time nodes if they have to be stopped.
*
* Arguments:
*   None.
* Return values:
*   None.
*/
void node_pause(void)
{
  DIAG(DIAG_LOW, "node_pause()");
}

/* This function is called for real-time nodes when they are resumed
* after having paused.
*
* Arguments:
*   None.
* Return values:
*   None.
*/
void node_resume(void)
{
  DIAG(DIAG_LOW, "node_resume()");
}

/* This function is called upon receival of external requests.
 *
 * Arguments:
 *   None.
 * Return values:
 *   None.
 */
void node_request(void)
{
  DIAG(DIAG_LOW, "node_request()");
}

/***********************************************************************
*
* Application-specific functions
*
***********************************************************************/

/* -----------------------------------------------------------------------------
* Function  : common_validation()
* Purpose   : Validate the common rules for all file types
* Arguments : value - field value
*             rej_store - rejection store
* Return    : PASS or FAIL
* --------------------------------------------------------------------------- */
int missing_validation(const char* value, const char* rej_store)
{
  if(strlen(value) == 0)
  {
    //i_reject(rej_store, value);
    return FAIL;
  }

  return PASS;
}
/* -----------------------------------------------------------------------------
* Function  : process_date()
* Purpose   : Transform input date field of epoch format into output of LMS
* Arguments : in_field_name - Name of the Input Field to be read
*             out_field_name - Name of the output field to be written
* Return    : PASS or FAIL
* --------------------------------------------------------------------------- */
int process_date(const char* in_field_name, const char* out_field_name, const char* out_format)
{
  DIAG(DIAG_HIGH, "process_date(): entered the function");  
  char inDate[20] = {'\0'};
  char outDate[25] = {'\0'};
  char rej_msg[100] = {'\0'};
  char* num_check_ptr;

  strncpy(inDate, iGet(in_field_name), 10);
  DIAG(DIAG_HIGH, "process_date(): %s: %s", in_field_name, inDate);
  
  /* Check if format is correct*/
  strtol(inDate, &num_check_ptr, 10);
  if(num_check_ptr == inDate || *num_check_ptr != '\0' || 0 != format_date(inDate, "%s", outDate, out_format, 22, 0))
  {
    sprintf(rej_msg, "Incorrect %s format", in_field_name);
    i_reject("INVALID", rej_msg);
    return FAIL;
  }
  DIAG(DIAG_HIGH, "process_date(): %s: %s", out_field_name, outDate);

  o_add_field(out_field_name, outDate);
  return PASS;
}
/* -----------------------------------------------------------------------------
* Function  : iGet()
* Purpose   : Check Existence and Read Input Field
* Arguments : NameOfField - Name of the Input Field to be read
* Return    : Value of Field
* --------------------------------------------------------------------------- */
const char * iGet(const char *NameOfField)
{
  if(i_field_exists(NameOfField))
  {
    DIAG(DIAG_HIGH, "iGet(): %-30s = [%s]", NameOfField, i_get(NameOfField));
  }
  else
  {
    DIAG(DIAG_HIGH, "iGet(): %-30s = <Does not exist>", NameOfField);
  }
  return i_get(NameOfField);
}

/* -----------------------------------------------------------------------------
* Function  : format_date()
* Purpose   : Change date format from an input to an output
* Arguments : src - input date, pattern - input format, target - char pointer to store output,  targetPattern - target format
* Return    : 0 if successful
* --------------------------------------------------------------------------- */
int format_date(char* src, char* pattern, char* target, const char* targetPattern, int targetLenth, int secondsToAdd)
{   
    if(strlen(src)==0)
    {
      DIAG(DIAG_HIGH, "format_date Failed. Empty source string.");
 
      return -2;
    }
    struct tm result;
	
    memset(&result, 0, sizeof(struct tm));
    if (strptime(src, pattern,&result) == NULL)
    {
          DIAG(DIAG_HIGH, "\nstrptime failed\n");
          DIAG(DIAG_HIGH, "'%s'\t'%s' '%s'\t'%s' '%s'\t'%s'\t'%s'\n",  "Failed. format_date returning", "source",src,
                                                                        "srcPattern",pattern,"target",target);

          target = NULL;
          return 1;
    }
    else
    {
          DIAG(DIAG_HIGH,"tm_hour:  %d\n",result.tm_hour);
          DIAG(DIAG_HIGH,"tm_min:  %d\n",result.tm_min);
          DIAG(DIAG_HIGH,"tm_sec:  %d\n",result.tm_sec);
          DIAG(DIAG_HIGH,"tm_mon:  %d\n",result.tm_mon);
          DIAG(DIAG_HIGH,"tm_mday:  %d\n",result.tm_mday);
          DIAG(DIAG_HIGH,"tm_year:  %d\n",result.tm_year);
          DIAG(DIAG_HIGH,"tm_yday:  %d\n",result.tm_yday);
          DIAG(DIAG_HIGH,"tm_wday:  %d\n",result.tm_wday);
          DIAG(DIAG_HIGH,"tm_isdst : %d\n", result.tm_isdst);
          
		  if(secondsToAdd > 0)
		  {
			result.tm_sec += secondsToAdd;
            timegm(&result);
		  }
          strftime(target,targetLenth,targetPattern , &result);
          
          DIAG(DIAG_HIGH, "'%s'\t'%s' '%s'\t'%s' '%s'\t'%s'\t'%s'\n",  "Success. format_date returning", "source",
                                                                  src, "targetPattern",targetPattern,"target",target);
          return 0;
    }
}

/* -----------------------------------------------------------------------------
* Function  : add_date_time()
* Purpose   : add common date and time fields to all output types
* Arguments : 
* Return    : PASS - FAIL
* --------------------------------------------------------------------------- */
int add_date_time()
{
  if(process_date("START_DATE_AND_TIME","DATE", "%d/%m/%Y") == FAIL)
  {
    return FAIL;
  }
  if(process_date("START_DATE_AND_TIME","HOUR", "%H:%M:%S") == FAIL)
  {
    return FAIL;
  }
  else
  {
    char out_time[15]= {'\0'};
    if(strlen(iGet("START_DATE_AND_TIME")) == 13)
    {
      DIAG(DIAG_HIGH, "add_date_time() Processing milliseconds");
      /*sprintf(out_time,"%s:%s", o_get("HOUR"), iGet("START_DATE_AND_TIME")+10);
      o_set_field("HOUR", out_time);*/
    }
  }
  return PASS;
}

/* -----------------------------------------------------------------------------
* Function  : process_eoIUPS()
* Purpose   : process eoIUPS record
* Arguments : 
* Return    : PASS - FAIL
* --------------------------------------------------------------------------- */
int process_eoIUPS()
{
  DIAG(DIAG_HIGH, "process_eoIUPS() Started Processing");

  if(missing_validation(i_get("IMSI"), "REJ_IMSI") == FAIL)
  {
    return FAIL;
  }

  if(add_date_time() == FAIL)
  {
    return FAIL;
  }

  o_add_field("IMSIA", i_get("IMSI"));
  o_add_field("IMEIA", i_get("IMEI"));
  o_add_field("TECHNOLOGY", "3G");
  o_add_field("COUNTRY", i_get("SUB_COUNTRY"));

  o_add_field("MSISDNA", i_get("MSISDN"));

  //Location
  if(strcmp(i_get("DATARECORDTYPE"), "1") == 0 || strcmp(i_get("DATARECORDTYPE"), "7") == 0)
  {
    if(strcmp(i_get("DATARECORDTYPE"),"1") == 0)
    {
      o_add_field("EVENT", "IMSI_ATTACH");
    }
    else if(strcmp(i_get("DATARECORDTYPE"), "7") == 0)
    {
      o_add_field("EVENT", "ROUTING_AREA_UPDATE");
    }
     o_add_field("CID", i_get("SAC_1"));
    audit_out_type("IUPS_LOCATION");
  }
  else if(strcmp(i_get("DATARECORDTYPE"), "2") == 0) //IMSI DETACH
  {
     o_add_field("CID", i_get("SAC_3"));
    audit_out_type("IUPS_IMSI-DETACH");
  }
  else
  {
    DIAG(DIAG_HIGH, "process_eoIUPS() No Match. Returning FAIL.");
    return FAIL;
  }
  
  DIAG(DIAG_HIGH, "process_eoIUPS() Finished Processing. Returning PASS.");

  return PASS;
}

/* -----------------------------------------------------------------------------
* Function  : process_eoIUCS()
* Purpose   : process eoIUCS record
* Arguments : 
* Return    : PASS - FAIL
* --------------------------------------------------------------------------- */
int process_eoIUCS()
{
  DIAG(DIAG_HIGH, "process_eoIUCS() Started Processing");
  char call_record[3][3] = {"1", "2", "12"};
  char sms_record[2][2] = {"3", "4"};


  if(missing_validation(i_get("IMSI"), "REJ_IMSI") == FAIL)
  {
    return FAIL;
  }

  

  if(val_in_array(i_get("DATARECORDTYPE"), sizeof(call_record)/sizeof(call_record[0]), call_record) == PASS)
  {
    if(strcmp(i_get("SUCCESS"), "1") == 0)
    {
      o_add_field("EL_TYPE_CDR", "APPEL-ABOUTI");
      audit_out_type("IUCS_APPEL-ABOUTI");
    }
    else if(strcmp(i_get("SUCCESS"), "0") == 0)
    {
      o_add_field("EL_TYPE_CDR", "APPEL-NON-ABOUTI");
      audit_out_type("IUCS_APPEL-NO-ABOUTI");
      o_add_field("CAUSE", i_get("CAUSE"));
    }
    return process_call(i_get("DATARECORDTYPE"), "MSISDN", "3G", "SAC_1", "SAC_3");
  }
  else if(val_in_array(i_get("DATARECORDTYPE"), sizeof(sms_record)/sizeof(sms_record[0]), sms_record) == PASS)
  {
    o_add_field("EL_TYPE_CDR", "SMS");
    audit_out_type("IUCS_SMS");
    return process_sms(i_get("DATARECORDTYPE"), "MSISDN", "3G", "SAC_1", "SAC_3");
  }
  else if(strcmp(i_get("DATARECORDTYPE"), "9") == 0 || strcmp(i_get("LOC_UPD_TYPE"), "2") == 0)
  {
    o_add_field("EL_TYPE_CDR", "LOCATION");
    audit_out_type("IUCS_LOCATION");
    return process_loc("3G", "SAC_1", "MSISDN");
  }
  else if(strcmp(i_get("DATARECORDTYPE"), "17") == 0)
  {
    o_add_field("EL_TYPE_CDR", "IMSI_Detach");
    audit_out_type("IUCS_IMSI-DETACH");
    return process_imsi_det("MSISDN", "3G", "SAC_3");
  }
  DIAG(DIAG_HIGH, "process_eoIUCS() Could not find matching DATARECORDTYPE");

  return FAIL;
}

int val_in_array(const char* val, int size, char arr[][size])
{
  DIAG(DIAG_HIGH, "val_in_array() Started Processing %s %d", val, size);
  
  for(int i = 0; i < size; i++)
  {
    if(strcmp(arr[i], val) == 0)
    {
      DIAG(DIAG_HIGH, "val_in_array() found %s", arr[i]);
      return PASS;
    }
  }
  DIAG(DIAG_HIGH, "val_in_array() not found %s", val);
  return FAIL;
}
void add_common_fields(const char* tech, const char* cidbegin, const char* cidend)
{
  o_add_field("DURATION", i_get("CONV_TIME"));
  o_add_field("IMSIA", i_get("IMSI"));
  o_add_field("IMEIA", i_get("IMEI"));
  o_add_field("CIDBEGIN", cidbegin);
  o_add_field("CIDEND", cidend);
  o_add_field("TECHNOLOGY", tech);//"2G"
}

int process_call(const char* data_rec_val, const char* msisdn_field, const char* tech, const char* cidbegin, const char* cidend)
{
  if(add_date_time() == FAIL)
  {
    return FAIL;
  }

  add_common_fields(tech, i_get(cidbegin), i_get(cidend));

  switch(atoi(data_rec_val))
  {
    case 1:
      o_add_field("COUNTRY", i_get("CALLED_COUNTRY"));
    case 12:
      o_add_field("DIRECTION", "Emis");
      o_add_field("MSISDNA", i_get(msisdn_field));
      o_add_field("MSISDNB", i_get("CALLED_NO"));
      break;
    case 2:
      o_add_field("DIRECTION", "Recu");
      o_add_field("MSISDNA", i_get("CALLING_NO"));
      o_add_field("MSISDNB", i_get(msisdn_field));
      o_add_field("COUNTRY", i_get("CALLING_COUNTRY"));
      break;
    default:
      break;
  }
  return PASS;
}

int process_sms(const char* data_rec_val, const char* msisdn_field, const char* tech, const char* cidbegin, const char* cidend)
{
  if(add_date_time() == FAIL)
  {
    return FAIL;
  }

  add_common_fields(tech, i_get(cidbegin), i_get(cidend));

  switch(atoi(data_rec_val))
  {
    case 3:
      o_add_field("DIRECTION", "Emis");
      o_add_field("MSISDNA", i_get(msisdn_field));

      if(strlen(i_get("CALLED_NO")) > 0)
        o_add_field("MSISDNB", i_get("CALLED_NO"));
      else if(strlen(i_get("CALLED_NO_STRING")) > 0)
        o_add_field("MSISDNB", i_get("CALLED_NO_STRING"));
      else if(strlen(i_get("GENERAL_CALLED_NUMBER")) > 0)
        o_add_field("MSISDNB", i_get("GENERAL_CALLED_NUMBER"));
      else if(strlen(i_get("SMS_TP_DA_ALPHANUMERIC")) > 0)
        o_add_field("MSISDNB", i_get("SMS_TP_DA_ALPHANUMERIC"));

      o_add_field("COUNTRY", i_get("CALLED_COUNTRY"));
      break;
    case 4:
      o_add_field("DIRECTION", "Recu");
      
      if(strlen(i_get("CALLING_NO")) > 0)
        o_add_field("MSISDNA", i_get("CALLING_NO"));
      else if(strlen(i_get("CLG_PARTY_NUM_STR")) > 0)
        o_add_field("MSISDNA", i_get("CLG_PARTY_NUM_STR"));
      else if(strlen(i_get("CALLING_NO_STRING")) > 0)
        o_add_field("MSISDNA", i_get("CALLING_NO_STRING"));
      else if(strlen(i_get("SMS_TP_OA_ALPHANUMERIC")) > 0)
        o_add_field("MSISDNA", i_get("SMS_TP_OA_ALPHANUMERIC"));
      
      o_add_field("MSISDNB", i_get(msisdn_field));
      o_add_field("COUNTRY", i_get("CALLING_COUNTRY"));
      break;
    default:
      break;
  }

  o_add_field("EL_CAUSE", i_get("CAUSE"));

  return PASS;
}

int process_loc(const char* tech, const char* cid, const char* msisdn_field)
{
  if(add_date_time() == FAIL)
  {
    return FAIL;
  }

  o_add_field("IMSIA", i_get("IMSI"));
  o_add_field("IMEIA", i_get("IMEI"));
  o_add_field("MSISDNA", i_get(msisdn_field));
  o_add_field("CID", i_get(cid));
  o_add_field("TECHNOLOGY", tech);
  o_add_field("COUNTRY", i_get("SUB_COUNTRY"));
  if(strcmp(i_get("LOC_UPD_TYPE"), "2") == 0)
    o_add_field("EVENT", "IMSI_ATTACH");
  else
    o_add_field("EVENT", "LOCATION_UPDATE");
  return PASS;
}
int process_imsi_det(const char* msisdn_field, const char* tech, const char* cid)
{
  char out_msisdna[20] = {0};
  if(add_date_time() == FAIL)
  {
    return FAIL;
  }
  sprintf(out_msisdna, "00%s", i_get(msisdn_field));
  o_add_field("MSISDNA", out_msisdna);
  o_add_field("IMSIA", i_get("IMSI"));
  o_add_field("IMEIA", i_get("IMEI"));
  o_add_field("CID", i_get(cid));
  o_add_field("TECHNOLOGY", tech);
  o_add_field("COUNTRY", i_get("SUB_COUNTRY"));

  return PASS;
}
/* -----------------------------------------------------------------------------
* Function  : process_eoAINT()
* Purpose   : process eoAINT record
* Arguments : 
* Return    : PASS - FAIL
* --------------------------------------------------------------------------- */
int process_eoAINT()
{
  DIAG(DIAG_HIGH, "process_eoAINT() Started Processing");
  char call_record[][3] = {"1", "2", "12"};
  char sms_record[][2] = {"3", "4"};


  if(missing_validation(i_get("IMSI"), "REJ_IMSI") == FAIL)
  {
    return FAIL;
  }

  if(val_in_array(i_get("DATARECORDTYPE"), sizeof(call_record)/sizeof(call_record[0]), call_record) == PASS)
  {
    if(strcmp(i_get("SUCCESS"), "1") == 0)
    {
      o_add_field("EL_TYPE_CDR", "APPEL-ABOUTI");
      audit_out_type("A_APPEL-ABOUTI");
    }
    else if(strcmp(i_get("SUCCESS"), "0") == 0)
    {
      o_add_field("EL_TYPE_CDR", "APPEL-NON-ABOUTI");
      audit_out_type("A_APPEL-NO-ABOUTI");
      o_add_field("CAUSE", i_get("CAUSE"));
    }
    return process_call(i_get("DATARECORDTYPE"), "MS_ISDN", "2G", "FIRST_CELL", "LAST_CELL");
  }
  else if(val_in_array(i_get("DATARECORDTYPE"), sizeof(sms_record)/sizeof(sms_record[0]), sms_record) == PASS)
  {
    o_add_field("EL_TYPE_CDR", "SMS");
    audit_out_type("A_SMS");
    return process_sms(i_get("DATARECORDTYPE"), "MS_ISDN", "2G", "FIRST_CELL", "LAST_CELL");
  }
  else if(strcmp(i_get("DATARECORDTYPE"), "5") == 0 || strcmp(i_get("LOC_UPD_TYPE"), "2") == 0)
  {
    o_add_field("EL_TYPE_CDR", "LOCATION");
    audit_out_type("A_LOCATION");
    return process_loc("2G", "FIRST_CELL", "MS_ISDN");
  }
  else if(strcmp(i_get("DATARECORDTYPE"), "16") == 0)
  {
    o_add_field("EL_TYPE_CDR", "IMSI_Detach");
    audit_out_type("A_IMSI-DETACH");
    return process_imsi_det("MS_ISDN", "2G", "LAST_CELL");
  }

  DIAG(DIAG_HIGH, "process_eoAINT() Could not find matching DATARECORDTYPE");
  
  return FAIL;
}

int process_eoTCAP()
{
  DIAG(DIAG_HIGH, "process_eoTCAP() Started Processing");
  if(missing_validation(i_get("IMSI"), "REJ_IMSI") == FAIL)
  {
    return FAIL;
  }
  DIAG(DIAG_HIGH, "process_eoTCAP() CALLING_NO comparison %d", strncmp(i_get("CALLING_NO"),"212", 3));
  DIAG(DIAG_HIGH, "process_eoTCAP() VLR_NUMBER comparison %d", strncmp(i_get("VLR_NUMBER"),"212", 3));
  if((strcmp(i_get("DATARECORDTYPE"), "1") == 0 || strcmp(i_get("DATARECORDTYPE"), "2") == 0) && 
        strncmp(i_get("CALLING_NO"), "212", 3) == 0 && strncmp(i_get("VLR_NUMBER"),"212", 3) != 0)
  {
    if(strcmp(i_get("SUCCESS"), "1") == 0)
    {
      char cellid[200] = {0};
      char capcell[100] = {0};
      
      sprintf(cellid, "%s-", i_get("PLMN_ID"));
      
      if(strlen(i_get("CAP_CELL_IDENTITY")) > 0)
      {
        sprintf(capcell, "%d", (int)strtol(i_get("CAP_CELL_IDENTITY"), NULL, 16));
        strcat(cellid, capcell);
      }
      add_common_fields("ALL Core", cellid, cellid);
      audit_out_type("TCAP-APPEL-ABOUTI");
    }
    else if(strcmp(i_get("SUCCESS"), "0") == 0)
    {
      audit_out_type("TCAP_APPEL-NO-ABOUTI");
      o_add_field("CAUSE", i_get("CAUSE"));
      o_add_field("CIDBEGIN", i_get("FIRST_CELL"));
      o_add_field("CIDEND", i_get("LAST_CELL"));
      add_common_fields("ALL Core", i_get("FIRST_CELL"), i_get("LAST_CELL"));
    }

    if(add_date_time() == FAIL)
    {
      return FAIL;
    }

    

    if(strcmp(i_get("DATARECORDTYPE"), "1") == 0)
    {
      o_add_field("DIRECTION", "Emis");
      o_add_field("MSISDNA", i_get("MSISDN"));
      o_add_field("MSISDNB", i_get("CALLED_NO"));
      o_add_field("COUNTRY", i_get("CALLED_COUNTRY"));
    }
    else if(strcmp(i_get("DATARECORDTYPE"), "2") == 0)
    {
      o_add_field("DIRECTION", "Recu");
      o_add_field("MSISDNA", i_get("CALLING_NO"));
      o_add_field("MSISDNB", i_get("MSISDN"));
      o_add_field("COUNTRY", i_get("CALLING_COUNTRY"));
      o_add_field("CAUSE", i_get("CAUSE"));
    }  

    DIAG(DIAG_HIGH, "process_eoTCAP() End Processing output");
    return PASS;  
  }  

  DIAG(DIAG_HIGH, "process_eoTCAP() End Processing No Match");
  return FAIL;
}

/* -----------------------------------------------------------------------------
* Function  : process_eoGB()
* Purpose   : process eoGB record
* Arguments : 
* Return    : PASS - FAIL
* --------------------------------------------------------------------------- */
int process_eoGB()
{
  DIAG(DIAG_HIGH, "process_eoGB() Started Processing.");

  

  DIAG(DIAG_HIGH, "process_eoGB() Finished Processing. Returning.");
  return FAIL;
}

int process_eoIPSDR()
{
  DIAG(DIAG_HIGH, "process_eoIPSDR() Started Processing.");
  audit_out_type("IPSDR_DATA");
  if(missing_validation(i_get("IMSI"), "REJ_IMSI") == FAIL)
  {
    return FAIL;
  }

  if(add_date_time() == FAIL)
  {
    return FAIL;
  }

  o_add_field("MSISDNA", i_get("MSISDN"));
  o_add_field("IMSIA", i_get("IMSI"));
  o_add_field("IMEIA", i_get("IMEI"));
  o_add_field("CID", i_get("LOCATION"));
  //o_add_field("TECHNOLOGY", "2G");
  o_add_field("APN", i_get("APN"));
  o_add_field("COUNTRY", i_get("SUB_COUNTRY"));

  int pdp_len = strlen(i_get("PDP_ADDRESS_V4"));
  DIAG(DIAG_HIGH, "process_eoIPSDR() PDP_ADDRESS_V4:%s", i_get("PDP_ADDRESS_V4"));

  if(pdp_len >= 8)
  {
    char hexipaddr[10] = {0};
    unsigned int a, b, c, d;
    strncpy(hexipaddr, i_get("PDP_ADDRESS_V4")+pdp_len-8, 8);
    if(sscanf(hexipaddr, "%2x%2x%2x%2x", &a, &b, &c, &d) != 4)
    {
      DIAG(DIAG_HIGH, "process_eoIPSDR() Unable to process PDP_ADDRESS_V4.");
    }
    else
    {
      char decipaddr[20] = {0};
      sprintf(decipaddr, "%u.%u.%u.%u", a, b, c, d);
      o_add_field("IPCLIENT", decipaddr);
    }
  }

  if(strcmp(i_get("RADIO_ACCESS_TYPE"), "1") == 0 || strcmp(i_get("RADIO_ACCESS_TYPE"), "5") == 0)
  {
    o_add_field("TECHNOLOGY", "3G");
  }
  else if(strcmp(i_get("RADIO_ACCESS_TYPE"), "2") == 0) 
  {
    o_add_field("TECHNOLOGY", "2G");
  }
  else if(strcmp(i_get("RADIO_ACCESS_TYPE"), "6") == 0) 
  {
    o_add_field("TECHNOLOGY", "4G");
  }
  else
  {
    o_add_field("TECHNOLOGY", "ALL_CORE");
  }

  DIAG(DIAG_HIGH, "process_eoIPSDR() Finished Processing. Returning.");
  return PASS;
}

/* -----------------------------------------------------------------------------
* Function  : process_eoMAP()
* Purpose   : process eoMAP record
* Arguments : 
* Return    : PASS - FAIL
* --------------------------------------------------------------------------- */
int process_eoMAP()
{
  DIAG(DIAG_HIGH, "process_eoMAP() Started Processing.");

  if(missing_validation(i_get("IMSI"), "REJ_IMSI") == FAIL)
  {
    return FAIL;
  }

  if(add_date_time() == FAIL)
  {
    return FAIL;
  }

  o_add_field("IMSIA", i_get("IMSI"));
  o_add_field("IMEIA", i_get("IMEI"));
  o_add_field("TECHNOLOGY", "2G");
  o_add_field("COUNTRY", i_get("NET_OPERATOR_NAME"));
  o_add_field("CIDBEGIN", i_get("FIRST_CELL"));
  o_add_field("CIDEND", i_get("LAST_CELL"));

  //SMS
  if(strcmp(i_get("DATARECORDTYPE"), "9") == 0 || strcmp(i_get("DATARECORDTYPE"), "10") == 0)
  {
    switch(atoi(i_get("DATARECORDTYPE")))
    {
      case 9:
        o_add_field("EL_DIRECTION", "Emis");
        o_add_field("MSISDNA", i_get("MS_ISDN"));

        if(strlen(i_get("TP_DA_ALPHANUM")) > 0)
          o_add_field("MSISDNB", i_get("TP_DA_ALPHANUM"));
        else if(strlen(i_get("TP_DA")) > 0)
          o_add_field("MSISDNB", i_get("TP_DA"));
        break;
      case 10:
        o_add_field("EL_DIRECTION", "Recu");
        
        if(strlen(i_get("TP_OA_ALPHANUM")) > 0)
          o_add_field("MSISDNA", i_get("TP_OA_ALPHANUM"));
        else if(strlen(i_get("TP_OA")) > 0)
          o_add_field("MSISDNA", i_get("TP_OA"));
        
        o_add_field("MSISDNB", i_get("MS_ISDN"));
        break;
      default:
        break;
    }
    
    audit_out_type("MAP_SMS");
  }
  else if(strcmp(i_get("DATARECORDTYPE"), "3") == 0 || strcmp(i_get("DATARECORDTYPE"), "4") == 0) //IMSI DETACH
  {
    if(strcmp(i_get("DATARECORDTYPE"), "3") == 0)
    {
      o_add_field("MSISDNA", i_get("GTCALLING_NO"));
    }
    else
    {
      if(strlen(i_get("TP_OA_ALPHANUM")) > 0)
        o_add_field("MSISDNA", i_get("TP_OA_ALPHANUM"));
      else
        o_add_field("MSISDNA", i_get("TP_OA"));
    }
    audit_out_type("MAP_IMSI-DETACH");
  }
  else
  {
    DIAG(DIAG_HIGH, "process_eoMAP() No Match. Returning FAIL.");
    return FAIL;
  }

  DIAG(DIAG_HIGH, "process_eoMAP() Finished Processing. Returning PASS.");
  return PASS;
}

/* -----------------------------------------------------------------------------
* Function  : process_eoLTE()
* Purpose   : process eoLTE record
* Arguments : 
* Return    : PASS - FAIL
* --------------------------------------------------------------------------- */
int process_eoLTE()
{
  DIAG(DIAG_HIGH, "process_eoLTE() Started Processing.");

  if(missing_validation(i_get("IMSI"), "REJ_IMSI") == FAIL)
  {
    return FAIL;
  }

  if(add_date_time() == FAIL)
  {
    return FAIL;
  }

  if(strcmp(i_get("DATARECORDTYPE"), "1") == 0 || strcmp(i_get("DATARECORDTYPE"), "3") == 0)
  {
    if(strcmp(i_get("DATARECORDTYPE"), "1") == 0)
      o_add_field("EVENT", "IMSI_ATTACH");
    else if(strcmp(i_get("DATARECORDTYPE"), "3") == 0)
      o_add_field("EVENT", "TRACKING_AREA_UPDATE");

    audit_out_type("S1_LOCATION");
  }
  else if(strcmp(i_get("DATARECORDTYPE"), "2") == 0)
  {
    audit_out_type("S1_IMSI-DETACH");
  }
  else
  {
    DIAG(DIAG_HIGH, "process_eoLTE() No Match. Returning FAIL.");
    return FAIL;
  }

  o_add_field("MSISDNA", i_get("MSISDN"));
  o_add_field("IMSIA", i_get("IMSI"));
  o_add_field("IMEIA", i_get("IMEI"));
  o_add_field("CID", i_get("SOURCE_CELL"));
  o_add_field("TECHNOLOGY", "2G");
  o_add_field("COUNTRY", i_get("SUB_COUNTRY"));

  DIAG(DIAG_HIGH, "process_eoLTE() Finished Processing. Returning PASS.");
  return PASS;
}

/* -----------------------------------------------------------------------------
* Function  : get_el_cause()
* Purpose   : Get eoAINT mapped string for EL_CAUSE
* Arguments : int cause - the value of incoming cause
* Return    : Mapped string
* --------------------------------------------------------------------------- */
char* get_el_cause(int cause)
{
  DIAG(DIAG_HIGH, "get_el_cause() input [%d]", cause);
  switch(cause)
  {
    case 89: return "E///SlcFR";
    case 90: return "E///SlcHR";
    case 91: return "E///SlcOLFR";
    case 92: return "E///SlcOLHR";
    default: return "";
  }   
}

/* -----------------------------------------------------------------------------
* Function  : get_el_IUPS_cause()
* Purpose   : Get eoIUPS/eoGB mapped string for EL_CAUSE
* Arguments : int cause - the value of incoming cause
* Return    : Mapped string
* --------------------------------------------------------------------------- */
char* get_el_IUPS_cause(int cause)
{
  DIAG(DIAG_HIGH, "get_el_IUPS_cause() input [%d]", cause);
  switch(cause)
  {
    case 8: return "SM_OperDetBarr";
    case 26: return "SM_InsRes";
    case 27: return "SM_MissUnApn";
    case 28: return "SM_UnPdpAddTyp";
    case 29: return "SM_UAutFail";
    case 30: return "SM_ActRejGgsn";
    case 21: return "SM_ActRejUnsp";
    case 32: return "SM_SOptNS";
    case 33: return "SM_ReqSONS";
    case 34: return "SM_SOptTempOut";
    case 35: return "SM_NSapiUsed";
    case 111: return "SM_PErrUnsp";
    default: return "";
  }   
}

/* -----------------------------------------------------------------------------
* Function  : get_AINT_start_event()
* Purpose   : Get eoAINT mapped string for start event
* Arguments : int serv_type - the value of incoming type
* Return    : Mapped string
* --------------------------------------------------------------------------- */
char* get_AINT_start_event(int serv_type)
{
  DIAG(DIAG_HIGH, "get_AINT_start_event() input [%d]", serv_type);
  switch(serv_type)
  {
    case 1: return "CMSRQ_NC_Phx";
    case 4: return "CMSRQ_SMS_Phx";
    case 8: return "CMSRQ_SS_Phx";
    case 2: return "CMSRQ_EC_Phx";
    default: return "";
  } 
}

/* -----------------------------------------------------------------------------
* Function  : get_data_rec_type()
* Purpose   : Get eoIUCS mapped string for data rec type
* Arguments : int rec_type - the value of incoming type
* Return    : Mapped string
* --------------------------------------------------------------------------- */
char* get_data_rec_type(int rec_type)
{
  DIAG(DIAG_HIGH, "get_data_rec_type() input [%d]", rec_type);
  switch(rec_type)
  {
    case 9: return "LUREQ_IA_Phx";
    case 1: return "CMSRQ_NC_Phx";
    case 12: return "CMSRQ_EC_Phx";
    case 10: return "CMSRQ_SS_Phx";
    case 3: return "CMSRQ_SMS_Phx";
    default: return "";
  } 
}

/* -----------------------------------------------------------------------------
* Function  : get_eoGB_rec_type()
* Purpose   : Get eoGB mapped string for data rec type
* Arguments : int rec_type - the value of incoming type
* Return    : Mapped string
* --------------------------------------------------------------------------- */
char* get_eoGB_rec_type(int rec_type)
{
  DIAG(DIAG_HIGH, "get_eoGB_rec_type() input [%d]", rec_type);
  switch(rec_type)
  {
    case 1: return "1";
    case 4: return "17";
    case 2: return "24";
    case 3: return "26";
    default: return "";
  } 
}

/* -----------------------------------------------------------------------------
* Function  : extract_add_field()
* Purpose   : Extract substring after the first occurence of a separator and add field to output record
* Arguments : field_val: input field value - sep: separator - sep_sz: length of sep string - out_field: name of output field
* Return    : void
* --------------------------------------------------------------------------- */
void extract_add_field(const char* field_val, const char* sep, int sep_sz, const char* out_field)
{
  char* sep_str;
  sep_str = strstr(field_val, sep);
  DIAG(DIAG_HIGH, "extract_add_field() input [%s]", field_val);
  if(sep_str != NULL)
  {
    o_add_field(out_field, sep_str+sep_sz);
  }
  else
  {
    o_add_field(out_field, field_val);
  }
}